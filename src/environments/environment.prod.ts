export const environment = {
  production: true,
  apiURL: 'https://communityliveservices.sequation.com',
  logo: `./assets/logo/bounce-back-logo.png`,
  appTitle: 'BBKW',
  appInsights: {
      instrumentationKey: '',
      disableExceptionTracking: true
  },
  azureBlobStorage: 'https://communitylivesa.blob.core.windows.net',
  fileSizeLimit: '1GB'
};
