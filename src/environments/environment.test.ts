export const environment = {
    production: true,
    apiURL: 'https://testservices.sequation.net',
    logo: `./assets/logo/bounce-back-logo.png`,
    appTitle: 'BBKW',
    appInsights: {
        instrumentationKey: '',
        disableExceptionTracking: true
    },
    azureBlobStorage: 'https://sequationtestsav2.blob.core.windows.net',
    fileSizeLimit: '1GB'
}
