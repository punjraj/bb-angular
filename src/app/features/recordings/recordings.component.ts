import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { MeetService } from '../service-user/meet/meet.service';
import { InPageNavService } from '../shared/components/in-page-nav/in-page-nav.service'
import * as moment from 'moment';
import { Utility } from '../../framework/utils/utility';




@Component({
  selector: 'app-recordings',
  templateUrl: './recordings.component.html',
  styleUrls: ['./recordings.component.scss']
})
export class RecordingsComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'kwFullName', 'suFullName', 'recordingStartDate', 'length', 'sessionState', 'actions'];
  dataSource = new MatTableDataSource();

  sortColumn = 'recordingStartDate';
  sortDirection = 'desc';
  pageSize = 10;
  filterBy = { keyword: '' };

  deleteReasonsRefData: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly meetService: MeetService,
    private readonly snackBarService: SnackBarService,
    private readonly inPageNavService: InPageNavService
  ) {
    this.inPageNavService.setNavItems(null);
  }

  ngOnInit() {
    this.resolveRecordings(this.filterBy);
  }


  resolveRecordings(filterBy) {

    let currentPageIndex = 0;
    if (!this.paginator) {
      currentPageIndex = 0;
    } else {
      currentPageIndex = this.paginator.pageIndex;
    }
    this.meetService.findAllRecordings(`${this.sortColumn},${this.sortDirection}`, currentPageIndex, this.pageSize, filterBy)
      .subscribe(recordings => {
        this.dataSource = recordings.content;
        this.paginator.length = recordings.totalElements;
        this.dataSource.sort = this.sort;
      }, error => {
        this.snackBarService.error(error.error.applicationMessage);
      });

  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(data => {
      this.sortColumn = data.active;
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
      this.resolveRecordings(this.filterBy);
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.resolveRecordings(this.filterBy);
        })
      )
      .subscribe();
  }

  onFilter(filterString: string) {
    this.filterBy.keyword = filterString;
    this.paginator.pageIndex = 0;
    this.resolveRecordings(this.filterBy);
  }

  playRecording(recordingId) {
    this.router.navigate(['./play'], { relativeTo: this.route, queryParams: { recordingId } });
  }

  calculateVideoLength(VLength) {
    if(Utility.isEmpty(VLength)){
      return '' ;
    }
    const hours = Math.floor(VLength / 3600);
    const  minutes = Math.floor((VLength - (hours * 3600)) / 60);
    const seconds = VLength - (hours * 3600) - (minutes * 60);
    const duration = hours.toString().padStart(2, '0') + ':' + 
    minutes.toString().padStart(2, '0') + ':' + 
    seconds.toString().padStart(2, '0');
    return duration;
  }

  
}
