import { TestBed } from '@angular/core/testing';

import { RecordingsService } from './recordings.service';

describe('RecordingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecordingsService = TestBed.get(RecordingsService);
    expect(service).toBeTruthy();
  });
});
