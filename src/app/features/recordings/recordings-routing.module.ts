import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayVideoRecordingsComponent } from './play-video-recordings/play-video-recordings.component';
import { RecordingsComponent } from './recordings.component';

const routes: Routes = [
  {
    path: '',
    component: RecordingsComponent,
    data: { title: 'Recordings', auth: [38] },
  },

  {
    path: 'play',
    component: PlayVideoRecordingsComponent,
    data: { auth: [38] },
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordingsRoutingModule { }
