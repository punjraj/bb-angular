import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecordingsRoutingModule } from './recordings-routing.module';
import { RecordingsComponent } from './recordings.component';
import { MaterialModule } from '../../framework/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlayVideoRecordingsComponent } from './play-video-recordings/play-video-recordings.component';
import { VideoPlayerModule } from '../shared/components/video-player/video-player.module';

@NgModule({
  declarations: [RecordingsComponent, PlayVideoRecordingsComponent],
  imports: [
    CommonModule,
    RecordingsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    VideoPlayerModule
  ]
})
export class RecordingsModule { }
