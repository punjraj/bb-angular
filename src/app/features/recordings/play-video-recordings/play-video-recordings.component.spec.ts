import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PlayVideoRecordingsComponent } from './play-video-recordings.component';

describe('PlayVideoRecordingsComponent', () => {
  let component: PlayVideoRecordingsComponent;
  let fixture: ComponentFixture<PlayVideoRecordingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayVideoRecordingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayVideoRecordingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
