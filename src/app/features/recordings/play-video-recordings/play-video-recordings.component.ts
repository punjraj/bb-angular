import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MeetService } from '../../service-user/meet/meet.service';
import { ServiceUserNavigation } from '../../service-user/service-user-nav';
import { InPageNavService } from '../../shared/components/in-page-nav/in-page-nav.service';

@Component({
  selector: 'app-play-video-recordings',
  templateUrl: './play-video-recordings.component.html',
  styleUrls: ['./play-video-recordings.component.scss']
})
export class PlayVideoRecordingsComponent implements OnInit {
  
  recordingId;
  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly meetService: MeetService
  ) {
    this.route.queryParams.subscribe(params => {
      this.recordingId = params.recordingId;
    });
  }

  ngOnInit() {
  }

  getUrl() {
    return this.meetService.getUrl(this.recordingId);
  }


}
