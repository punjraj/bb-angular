import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { forkJoin } from 'rxjs';
import { ManageUsersService } from './../manage-users.service';
import { IUser } from './../user.interface';
import { SnackBarService } from './../../../framework/service/snack-bar.service';
import { ElementSchemaRegistry } from '@angular/compiler';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  loggedInUserRole: number;
  user: IUser
  accountTypeList: any[];
  id: any;

  constructor(
    private readonly manageUsersService: ManageUsersService,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    private readonly snackBarService: SnackBarService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.id = params.id;
    });
    this.loggedInUserRole = this.manageUsersService.resolveLoggedInUserRole();

    forkJoin([
      this.manageUsersService.getRoles(),
      this.manageUsersService.getUserDetails(this.id)
    ]).subscribe((data: any) => {
      data[0].forEach(role => {
        Object.assign(role, { id: role.roleId });
        Object.assign(role, { value: role.roleName });
        this.accountTypeList = data[0];
      });
      this.user = data[1];
    },
      error => {
        this.location.back();
        this.snackBarService.error(error.error.applicationMessage);
      });

  }
}
