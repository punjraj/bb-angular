import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { tap } from 'rxjs/operators';

import { ManageUsersService } from './manage-users.service';
import { IUser } from './user.interface';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { AppConfirmService } from '../../framework/components/app-confirm/app-confirm.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit, AfterViewInit {

  loggedInUserRole: number;

  displayedColumns: string[] = ['fullName', 'userName', 'accountType', 'accountState', 'actions'];
  dataSource = new MatTableDataSource<IUser>();

  sortColumn = 'fullName';
  sortDirection = 'asc';
  pageSize = 10;
  filterBy = { 'keyword': '' , 'refUserType':'SF' };

  deleteReasonsRefData: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private readonly manageUsersService: ManageUsersService,
    private readonly snackBarService: SnackBarService,
    private readonly appConfirmService: AppConfirmService
  ) { }

  ngOnInit() {
    this.loggedInUserRole = this.manageUsersService.resolveLoggedInUserRole();
    this.resolveUsers(this.filterBy);
    this.resolveDeleteReasonsRefData();
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(data => {
      this.sortColumn = data.active;
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
      this.resolveUsers(this.filterBy);
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.resolveUsers(this.filterBy);
        })
      )
      .subscribe();
  }

  resolveUsers(filterBy) {
    let currentPageIndex = 0;
    if (!this.paginator) {
      currentPageIndex = 0;
    } else {
      currentPageIndex = this.paginator.pageIndex;
    }
    
    this.manageUsersService.findAllPaginated(`${this.sortColumn},${this.sortDirection}`, currentPageIndex, this.pageSize, filterBy)
      .subscribe(allUsersData => {
        this.dataSource = allUsersData.content;
        this.paginator.length = allUsersData.totalElements;
        this.dataSource.sort = this.sort;
      }, error => {
        this.snackBarService.error(error.error.applicationMessage);
      });
  }

  onFilter(filterString: string) {
    this.filterBy.keyword = filterString;
    this.paginator.pageIndex = 0;
    this.resolveUsers(this.filterBy);
  }

  resolveDeleteReasonsRefData() {
    this.manageUsersService.getDeleteReasonsRefData().subscribe(
      data => this.deleteReasonsRefData = data,
      error => this.snackBarService.error(error.error.applicationMessage)
    );
  }

  onDeleteClicked(elementId) {

    const dialogRef = this.appConfirmService.confirm({
      title: `Delete User`,
      message: `Are you sure you want to delete user?`,
      showTextField: false,
      placeholderTextField: '',
      showSelectField: true,
      placeholderSelectField: `Select a reason for delete`,
      optionsSelectField: this.deleteReasonsRefData
    });

    dialogRef.subscribe(result => {

      if (result) {

        this.manageUsersService
          .deleteUser(JSON.stringify({ 'deletionReasonId': result, 'id': elementId })).subscribe(
            response => {
              this.snackBarService.success(`The user is deleted`);
              this.paginator.firstPage();
              this.resolveUsers(this.filterBy);
            },
            error => this.snackBarService.error(error.error.applicationMessage)
          );
      }
    });
  }
  
  isDeleteDisabled(id) {
    const loggedInUserId =  JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userId;
    if(id === loggedInUserId) {
      return true
    }
    return false
    
    }
}
