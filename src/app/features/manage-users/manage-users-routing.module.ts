import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ManageUsersComponent } from './manage-users.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { CanDeactivateGuard } from './../../framework/guards/can-deactivate/can-deactivate.guard';


const routes: Routes = [

  {
    path: '',
    component: ManageUsersComponent,
    data: { title: 'Title for Manage Staff'},
    // canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'view-user/:id',
    component: ViewUserComponent,
    data: { title: 'View Staff'},
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
    data: { title: 'Edit Staff' }
  },
  {
    path: 'new-user',
    component: EditUserComponent,
    data: { title: 'Create New Staff'},
    // canDeactivate: [CanDeactivateGuard]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageUsersRoutingModule { }
