import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BaseUrl } from '../../framework/constants/url-constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManageUsersService {

  constructor(private readonly http: HttpClient) { }

  getUserRoles() {
    const href = `${BaseUrl.USER}/user/roles`;
    return this.http.get<any>(href);
  }

  getUserTypes() {
    const href = `${BaseUrl.USER}/user/userType`;
    return this.http.get<any>(href);
  }

  createUser(payload) {
    return this.http.post<any>(`${BaseUrl.USER}/user`, payload);
  }

  findAllPaginated(sort: string, page: number, size: number, body: any): Observable<any> {
    const href = `${BaseUrl.USER}/user/search`;
    return this.http.post<any>(href, body, {
      params: new HttpParams()
        .set('sort', sort.toString())
        .set('page', page.toString())
        .set('size', size.toString())
    });
  }

  getUserDetails(id) {
    const href = `${BaseUrl.USER}/user/${id}`;
    return this.http.get<any>(href);
  }


  updateUser(payload) {
    return this.http.put(`${BaseUrl.USER}/user`, payload, { responseType: 'text' });
  }

  resolveLoggedInUserRole() {
    return JSON.parse(atob(localStorage.getItem('token').split('.')[1])).roleId;
  }

  getRoles(){
    const href = `${BaseUrl.USER}/user/roles`;
    return this.http.get<any>(href);
  }

  getDeleteReasonsRefData() {
    const href = `${BaseUrl.USER}/user/refData`;
    return this.http.get<any>(href);
 }


 deleteUser(payload) {
  const href = `${BaseUrl.USER}/user/deleteUser`;
  return this.http.post(href,  payload);
}

}
