import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, AbstractControl, Validators } from '@angular/forms';

import { IUser } from './../user.interface';
import { ManageUsersService } from './../manage-users.service';
import { SnackBarService } from './../../../framework/service/snack-bar.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  routeIntent: string;
  userForm: FormGroup;
  accountType;
  public rolesObjCtrl: FormControl = new FormControl();
  formData: IUser;
  telephoneCodePattern = /^[0-9]*$/;
  emailPattern = /^[a-z0-9._%+'-]+@[a-z0-9.-]+\.[a-z]{2,}$/i;
  usernamePattern = /^[a-zA-Z_0-9]*(\.[a-zA-Z_0-9]+)?$/;
  firstNamePattern = `([a-zA-Z]{1,})`;
  lastNamePattern = `([a-zA-Z]{1,}(\\s?|'?|-?)[a-zA-Z]{1,})`;
  titles = [
    'Mr', 'Mrs', 'Miss', 'Ms', 'Mx', 'Sir', 'Dr', 'Prof', 'Lady', 'Rev', 'Lord', 'Maj',
    'Capt', 'Pte', 'Sgt', 'Ssgt', 'Lt', 'Col', 'Brig', 'Cpl', 'Lcpl', '2Lt', 'WO1', 'WO2', 'Master', 'Other', 'N/A'
  ];
  roles = [];
  newUserTypeId: number;
  manageUserUrl = '/manage-users';
  constructor(
    private readonly fb: FormBuilder,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly location: Location,
    private readonly manageUsersService: ManageUsersService,
    private readonly snackBar: SnackBarService
  ) { }

  ngOnInit() {
    this.resolveRouteIntent();
    this.resolveRoles();
    this.initDataForm();
  
  }

  resolveRouteIntent() {
    if (this.activatedRoute.snapshot.data['title'] === 'Edit Staff') {
      this.routeIntent = 'editUser';
    } else {
      this.routeIntent = 'newUser';
      this.manageUsersService.getUserTypes().subscribe(userTypes => {
        this.newUserTypeId = userTypes[0].id;
      })
    }
  }

  resolveRoles() {
    this.manageUsersService.getUserRoles().subscribe(roles => {
      this.roles = roles;
    })
  }

  initDataForm() {
    this.createUserForm();
  }

  createUserForm() {

    this.userForm = this.fb.group({
      id: '',
      accountType: [null, [Validators.required, Validators.maxLength(35)]],
      accountState: [true],
      title: [null],
      firstName: ['', [Validators.required, Validators.maxLength(100), Validators.pattern(this.firstNamePattern)]],
      lastName: ['', [Validators.required, Validators.maxLength(100), Validators.pattern(this.lastNamePattern)]],
      userName: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(this.usernamePattern)]],
      emailAddress: ['', [Validators.required, Validators.maxLength(100), Validators.pattern(this.emailPattern)]],
      phoneNumber: [null, [Validators.maxLength(18), Validators.pattern(this.telephoneCodePattern)]],
      userTypeId: null,
    });

    if (this.routeIntent === 'editUser') {
      this.userForm.controls.userName.disable();
      this.userForm.controls.accountType.disable();
      this.activatedRoute.params.subscribe((params: any) => {
      forkJoin([
          this.manageUsersService.getUserRoles(),
          this.manageUsersService.getUserDetails(params.id)
        ]).subscribe(result => {
          this.roles = result[0];
          const userData = result[1];
          this.roles.forEach(role => {
            if (role.roleId === userData.accountType) {
              this.accountType = role.roleName;
            }
          })
          this.userForm.setValue({
            id: userData.id,
            accountType: userData.accountType,
            accountState: userData.accountState,
            title: userData.title,
            firstName: userData.firstName,
            lastName: userData.lastName,
            userName: userData.userName,
            emailAddress: userData.emailAddress,
            phoneNumber: userData.phoneNumber,
            userTypeId: userData.userTypeId,
          })
        })
      })
    }
  }

  onSubmit(userForm) {
    if (this.routeIntent === 'newUser') {
      this.onSaveNewUser(userForm);
    } else if (this.routeIntent === 'editUser') {
      this.onUpdateUser(userForm);
    }
  }

  onUpdateUser(userForm) {
    const payload = this.formatPayload(userForm);
    this.manageUsersService.updateUser(payload).subscribe(resp => {
      this.snackBar.success(`Success! The ${payload.userName} user is saved`);
      this.router.navigate([this.manageUserUrl]);
    }, error => {
      this.snackBar.error(error.error.applicationMessage);
    })
  }

  onSaveNewUser(userForm) {
    const payload = this.formatPayload(userForm);
    this.manageUsersService.createUser(payload).subscribe(resp => {
      this.snackBar.success(`Success! The ${payload.userName} user is saved`);
      this.router.navigate([this.manageUserUrl]);
    }, error => {
      this.snackBar.error(error.error.applicationMessage);
    })
  }

  showFormSuccessMessageThenRouteUser(resp) {
    this.snackBar.success(resp.message.applicationMessage);
    this.router.navigate([this.manageUserUrl]);
  }

  formatPayload(userForm) {
    const userFormRaw = userForm.getRawValue();
    return {
      ...userFormRaw,
      accountType: this.routeIntent === 'editUser' ? userForm.controls.accountType.value : userFormRaw.accountType.roleId,
      userTypeId: this.routeIntent === 'newUser' ? this.newUserTypeId : userForm.controls.userTypeId.value,
      firstName: userFormRaw.firstName.trim(),
      lastName: userFormRaw.lastName.trim()
    }
  }

}
