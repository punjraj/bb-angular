import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContentManagementSteps } from '../content.steps';
import { FormControl, Validators } from '@angular/forms';
import { ContentManagementService } from '../content-management.service';
import { IEstablishment, ILotname } from '../content.interface';
import { StepperNavigationService } from '../../shared/components/stepper-navigation/stepper-navigation.service';
import { SnackBarService } from '../../../framework/service/snack-bar.service';


@Component({
  selector: 'app-edit-content-establishment',
  templateUrl: './edit-content-establishment.component.html',
  styleUrls: ['./edit-content-establishment.component.scss']
})
export class EditContentEstablishmentComponent implements OnInit {

  public contentId: number;
  public selected: FormControl = new FormControl([], [Validators.required]);

  public lotNames: ILotname[] = [];
  public organizations: IEstablishment[] = [];
  public preSelected: IEstablishment[] = [];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly snackBarService: SnackBarService,
    private readonly contentSteps: ContentManagementSteps,
    private readonly stepperNavService: StepperNavigationService,
    private readonly contentService: ContentManagementService
  ) {
    this.stepperNavService.stepper(this.contentSteps.stepsConfig);
  }

  ngOnInit() {
    this.initEstablishmentForm();
  }

  initEstablishmentForm() {
    this.route.queryParams.subscribe(params => {
      if (params.id) {
        this.contentId = +params.id;
        this.initOrganizationList();
      } else {
        this.router.navigateByUrl('/content-management');
      }
    });
  }

  async getSelectedOrgs() {
    this.selected.setValue([]);
    await this.contentService.getSelectedOrganizations(this.contentId).toPromise()
      .then(data => {
        this.preSelected = data.orgUnitIds.map((id: number) => {
          return { id: id };
        });
        this.selected.setValue(data.orgUnitIds);
      });
  }

  async initOrganizationList() {
    await this.getSelectedOrgs();
    this.contentService.getAllOrganizations().subscribe((res: any) => {
      this.lotNames = res.lotNames;
      this.organizations = res.organizations;
    });
  }

  public saveOrganizations() {
    const content = {
      contentId: this.contentId,
      orgUnitIds: this.selected.value
    }
    this.contentService.saveContentOrganizations(content).subscribe(
      response => {
        this.router.navigate(['/content-management/keyword'], { queryParams: { id: this.contentId } });
        this.snackBarService.success(response.applicationMessage);
      }, error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
      });
  }

  updateOrganizations(data: any[]) {
    this.selected.setValue(Object.assign(data.map(org => org.id)));
  }

}