import { ViewContentComponent } from './view-content/view-content.component';
import { ContentManagementComponent } from './content-management.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditContentComponent } from './edit-content/edit-content.component';
import { EditContentKeywordsComponent } from './edit-content-keywords/edit-content-keywords.component';
import { EditContentEstablishmentComponent } from './edit-content-establishment/edit-content-establishment.component';
import { EditContentImageComponent } from './edit-content-image/edit-content-image.component';
import { AuthorizationGuard } from '../../framework/guards/authorization.guard';
import { AuthenticationGuard } from '../../framework/guards/authentication.guard';
import { ContentUploadComponent } from './content-upload/content-upload.component';
import { EditContentModuleComponent } from './edit-content-module/edit-content-module.component';
import { UploadExternalContentComponent } from './content-upload/upload-external-content/upload-external-content.component';
import { InternalContentComponent } from './content-upload/internal-content/internal-content.component';
import { ExternalContentComponent } from './content-upload/external-content/external-content.component';
import { DeactivateGuard } from '../../framework/guards/deactivate.guard';


const routes: Routes = [
  {
    path: '',
    component: ContentManagementComponent,
    data: { title: 'Manage Content', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'content',
    component: EditContentComponent,
    data: { title: 'Content Description', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'establishment',
    component: EditContentEstablishmentComponent,
    data: { title: 'Content Teams', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'keyword',
    component: EditContentKeywordsComponent,
    data: { title: 'Content Keywords', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'image',
    component: EditContentImageComponent,
    data: { title: 'Content Image', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'upload',
    component: ContentUploadComponent,
    data: { title: 'Content Upload', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'upload/internal/module',
    component: EditContentModuleComponent,
    data: { title: 'Content Module', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'view-contents',
    component: ViewContentComponent,
    data: { title: 'Content View', auth:[13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {path: 'upload/internal',
  component: InternalContentComponent,
  data: {title: 'Internal Content', auth: [13]},
  canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'upload/external',
    component: UploadExternalContentComponent,
    data: {title: 'External Content', auth: [13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'upload/external/view',
    component: ExternalContentComponent,
    data: {title: 'External Content', auth: [13]},
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  } 
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentManagementRoutingModule { }
