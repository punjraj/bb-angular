import { FilterPipeModule } from '../../framework/pipes/filter.module';
import { ContentModuleComponent } from './content-module/content-module.component';
import { AppConfirmModule } from './../../framework/components/app-confirm/app-confirm.module';
import { ContentManagementService } from './content-management.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ContentManagementRoutingModule } from './content-management-routing.module';
import { ContentManagementComponent } from './content-management.component';
import { EditContentComponent } from './edit-content/edit-content.component';
import { ViewContentComponent } from './view-content/view-content.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StepperNavigationModule } from '../../features/shared/components/stepper-navigation/stepper-navigation.module';
import { StepperNavigationService } from '../../features/shared/components/stepper-navigation/stepper-navigation.service';
import { ContentManagementSteps } from './content.steps';
import { EditContentEstablishmentComponent } from './edit-content-establishment/edit-content-establishment.component';
import { EditContentKeywordsComponent } from './edit-content-keywords/edit-content-keywords.component';
import { EditContentImageComponent } from './edit-content-image/edit-content-image.component';
import { ContentUploadComponent } from './content-upload/content-upload.component';
import { EditContentModuleComponent } from './edit-content-module/edit-content-module.component';
import { materialize } from 'rxjs/operators';
import { MaterialModule } from '../../framework/material/material.module';
import { FeatureAllowModule } from '../shared/components/directives/features-allow.module';
import { FileUploadModule } from '../shared/components/file-upload/file-upload.module';
import { ImageViewModule } from '../shared/components/image-viewer/image-viewer.module';
import { DocViewerModule } from '../shared/components/doc-viewer/doc-viewer.module';
import { VideoPlayerModule } from '../shared/components/video-player/video-player.module';
import { OrgSearchFilterModule } from '../shared/components/org-search-filter/org-search-filter.module';
import { ExternalContentComponent } from './content-upload/external-content/external-content.component';
import { InternalContentComponent } from './content-upload/internal-content/internal-content.component';
import { UploadExternalContentComponent } from './content-upload/upload-external-content/upload-external-content.component';


@NgModule({
  declarations: [
    ContentManagementComponent,
    EditContentComponent,
    ViewContentComponent,
    EditContentEstablishmentComponent,
    EditContentKeywordsComponent,
    EditContentImageComponent,
    ContentUploadComponent,
    EditContentModuleComponent,
    ContentModuleComponent,
    ExternalContentComponent,
    InternalContentComponent,
    UploadExternalContentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContentManagementRoutingModule,
    MaterialModule,
    FeatureAllowModule,
    StepperNavigationModule,
    FeatureAllowModule,
    FileUploadModule,
    DragDropModule,
    AppConfirmModule,
    DocViewerModule,
    VideoPlayerModule,
    OrgSearchFilterModule,
    FilterPipeModule,
    ImageViewModule
  ],
  entryComponents: [
    ContentModuleComponent
  ],
  providers: [
    ContentManagementService,
    StepperNavigationService,
    ContentManagementSteps
  ],
})
export class ContentManagementModule { }
