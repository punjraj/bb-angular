import { Injectable } from '@angular/core';

@Injectable()
export class ContentManagementSteps {

    stepsConfig = [
        {
            name: 'Description',
            state: '/content-management/content',
            id: 'D'
        },
        {
            name: 'Teams',
            state: '/content-management/establishment',
            id: 'E'
        },
        {
            name: 'Keywords',
            state: '/content-management/keyword',
            id: 'K'
        },
        {
            name: 'Image',
            state: '/content-management/image',
            id: 'I'
        },
        {
            name: 'Upload',
            state: '/content-management/upload',
            id: 'U'
        }
    ];
}