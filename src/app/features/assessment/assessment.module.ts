import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssessmentComponent } from './assessment.component';
import { AssessmentRoutingModule } from './assessment.routing';
import { AssessmentService } from './assessment.service';
import { EditAssessmentComponent } from './edit-assessment/edit-assessment.component';
import { ViewAssessmentComponent } from './view-assessment/view-assessment.component';
import { AssessmentsCustomLocalisations } from './assessment.en.localisation';
import { PublishAssessmentComponent } from './publish-assessment/publish-assessment.component';
import { PaginationService } from './publish-assessment/publish-assessment.pagination';
import { SharedModule } from '../..//framework/shared/shared.module';
import { MaterialModule } from '../../framework/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AssessmentRoutingModule
  ],
  declarations: [
    AssessmentComponent,
    EditAssessmentComponent,
    ViewAssessmentComponent,
    PublishAssessmentComponent,
  ],
  providers: [AssessmentService,
    AssessmentsCustomLocalisations, PaginationService],
  entryComponents: [ ]
})
export class AssessmentModule { }
