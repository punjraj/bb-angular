export interface IPublishAssessment {
    id: number,
    fullName: string,
    PRN: string,
    checked: boolean;
}
