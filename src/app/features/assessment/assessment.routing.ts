import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssessmentComponent } from './assessment.component';
import { EditAssessmentComponent } from './edit-assessment/edit-assessment.component';
import { ViewAssessmentComponent } from './view-assessment/view-assessment.component';
import { AuthorizationGuard } from '../../framework/guards/authorization.guard';
import { PublishAssessmentComponent } from './publish-assessment/publish-assessment.component';



 const AssessmentRoutes: Routes = [
  {
    path: '',
    component: AssessmentComponent,
    data: { title: 'Assessment Builder', auth: [6] }
  },
  {
    path: 'edit-assessment/:assessmentTemplateId',
    component: EditAssessmentComponent,
    canActivate: [AuthorizationGuard],
    data: { title: 'Edit Assessment', auth: [6, 3]  }
  },
  {
    path: 'view-assessment/:assessmentTemplateId',
    component: ViewAssessmentComponent,
    canActivate: [AuthorizationGuard],
    data: { title: 'View Assessment', auth: [6, 2] }
  },
  {
    path: 'new-assessment',
    component: EditAssessmentComponent,
    canActivate: [AuthorizationGuard],
    data: { title: 'Add New Assessment', auth: [6, 1] }
  },
  {
    path: 'publish-assessment',
    component: PublishAssessmentComponent,
    canActivate: [AuthorizationGuard],
    data: { title: 'Publish Assessment', auth: [6, 6] }
  },
];
@NgModule({
  imports: [RouterModule.forChild(AssessmentRoutes)],
  exports: [RouterModule]
})
export class AssessmentRoutingModule { }