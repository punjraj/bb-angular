import { Component, OnInit, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { AssessmentService } from '../assessment.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

import * as SurveyKo from 'survey-knockout';
import * as SurveyEditor from 'survey-creator';
import * as widgets from 'surveyjs-widgets';
import { AssessmentsCustomLocalisations } from '../assessment.en.localisation';
import { AppConfirmService } from '../../../framework/components/app-confirm/app-confirm.service';

const defaultQuestionTypes = [ 'text','checkbox','dropdown', 'radiogroup','rating', 'imagepicker', 'expression', 'html', 'comment', widgets.bootstrapslider(SurveyKo)];

const editorOptions = {
  showEmbededSurveyTab: false,
  generateValidJSON: true,
  showJSONEditorTab: false,
  showPropertyGrid: false,
  showTestSurveyTab: false,
  questionTypes: defaultQuestionTypes
};

const ckEditorModal = {
  afterRender(modalEditor, htmlElement) {
    if (window['CKEDITOR']) {
      const editor = window['CKEDITOR'].replace(htmlElement);
      editor.on('change', function () {
        modalEditor.editingValue = editor.getData();
      });
      editor.setData(modalEditor.editingValue);
    }
  },
  destroy(modalEditor, htmlElement) {
    if (window['CKEDITOR']) {
      const instance = window['CKEDITOR'].instances[htmlElement.id];
      if (instance) {
        instance.removeAllListeners();
        window['CKEDITOR'].remove(instance);
      }
    }
  }
};

const ckEditorUrl = './assets/scripts/ckeditor/ckeditor.js';

@Component({
  selector: 'app-edit-assessment',
  templateUrl: './edit-assessment.component.html',
  styleUrls: ['./edit-assessment.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class EditAssessmentComponent implements OnInit, AfterViewInit, OnDestroy {
  editor: SurveyEditor.SurveyEditor;
  json;
  surveyItem;
  assessmentTemplate;
  private assessmentTemplateId: any;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly assessmentService: AssessmentService,
    private readonly snack: MatSnackBar,
    private readonly assessmentsCustomLocalisations: AssessmentsCustomLocalisations,
    private readonly _onConfirmService: AppConfirmService,
  ) { }

  ngOnInit() {
    this.loadCkEditorScript();
  }

  ngOnDestroy(): void {
    this.unloadCkEditorScript();
  }

  ngAfterViewInit() {
    this.getAssessmentJson();

  }

  getAssessmentJson() {
    this.route.params.subscribe((params: any) => {
      if (params.hasOwnProperty('assessmentTemplateId')) {
        this.assessmentTemplateId = params['assessmentTemplateId'];
        this.assessmentService.getAssessment(this.assessmentTemplateId).subscribe((data: any) => {
          this.assessmentTemplate = data;
          this.json = JSON.parse(this.assessmentTemplate.questionJson);
        },
          error => this.snack.open(error.error.applicationMessage, 'Dismiss', { duration: 4000 }),
          () => { this.renderEditAssessment(this.json); }
        );
      } else {
        this.renderEditAssessment(this.json);
      }
    });
  }

  renderEditAssessment(json) {

    SurveyEditor.editorLocalization.locales['en_uk'] = this.assessmentsCustomLocalisations.enUKStrings;
    SurveyEditor.editorLocalization.currentLocale = 'en_uk';

    const defaultThemeColorsEditor = SurveyEditor
    .StylesManager
    .ThemeColors['default'];
    defaultThemeColorsEditor['$primary-color'] = '#2b769b';
    defaultThemeColorsEditor['$secondary-color'] = '#2b769b';
    SurveyEditor.StylesManager.applyTheme();
    SurveyKo.JsonObject.metaData.addProperty("itemvalue", {name: "score:number"});

    this.editor = new SurveyEditor.SurveyEditor(
      'surveyEditorContainer',
      editorOptions
    );

    SurveyEditor.SurveyPropertyModalEditor.registerCustomWidget(
      'html',
      ckEditorModal
    );

    SurveyKo.JsonObject.metaData.addProperty('page', 'popupdescription:text');
    this.editor.text = JSON.stringify(json);
    this.editor.saveSurveyFunc = this.onSaveAssessment;

    this.editor.toolbarItems.push({
      id: 'back_button',
      visible: true,
      title: 'Exit',
      action: () => this.router.navigate(['./assessment-builder/'])
    });

  }

  onSaveAssessment = () => {
    const obj = {
      template: JSON.parse(this.editor.text),
      userName: JSON.parse(atob(localStorage.getItem('token').split('.')[1])).fullName,
      assessmentTemplateId: this.assessmentTemplateId
    };
    let isScoreElementAvailable = false;
    const questions = JSON.parse(this.editor.text);
    const keys = Object.keys(questions);
    keys.forEach(element => {
      if('pages' === element){
        const pages = questions.pages;
        pages.forEach(page => {
          const  elements = page.elements;
          elements.forEach(question => {
           if(question.type === 'expression' && 'score' === question.name){
              isScoreElementAvailable = true;
           }
          })
        });
      }});

    if(!isScoreElementAvailable){
      const dialogRef = this._onConfirmService.confirm({
        title: `Scoring expression not selected`,
        message: `You have not selected scoring element, do you want to continue without scoring expression?`,
        okButtonName: 'Yes',
        cancelButtonName: 'No'
      });

      dialogRef.subscribe(output => {
        if (output) {
         this.saveTemplate(obj);
        }
      });
    } else{
      this.saveTemplate(obj);
    }
    
  }

  saveTemplate(obj){
    if (JSON.stringify(obj.template !== JSON.stringify(this.json))) {
      this.assessmentService.createAssessment(obj).subscribe(
        (data: any) => {
          if(obj.assessmentTemplateId){
            this.snack.open('Assessment Template Updated!', 'Dismiss', { duration: 4000 });
          }else{
            this.snack.open('Assessment Template Saved!', 'Dismiss', { duration: 4000 });
          }
          
          this.router.navigate(['./assessment-builder/']);
        },
        (error: any) => {
          this.snack.open(error.error.applicationMessage, 'Dismiss', { duration: 4000 });
        }
      );
    } else {
      this.snack.open('No updates to save!', 'Dismiss', { duration: 4000 });
    }
  }

  private loadCkEditorScript() {
    const node = document.createElement('script');
    node.src = ckEditorUrl;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    node.id = 'CkEditorScript';
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  private unloadCkEditorScript() {
    [].slice.call(document.querySelectorAll('script'), 0)
   .filter(script => script.src.includes('ckeditor'))
   .forEach(script => script.parentNode.removeChild(script));
  }

}
