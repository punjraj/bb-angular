import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssessmentService } from '../assessment.service';
import * as Survey from 'survey-angular';
import * as widgets from 'surveyjs-widgets';

widgets.bootstrapslider(Survey);


@Component({
  selector: 'app-view-assessment',
  templateUrl: './view-assessment.component.html',
  styleUrls: ['./view-assessment.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ViewAssessmentComponent implements OnInit {

  json;
  surveyItem;
  assessmentTemplate;
  assessmentName;
  assessmentTemplateId: any;
  isCompleted: boolean = false;
  private score: number = 0;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly assessmentService: AssessmentService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.getAssessmentJson();
  }

  getAssessmentJson() {
    this.route.params.subscribe((params: any) => {
      if (params.hasOwnProperty('assessmentTemplateId')) {
        this.assessmentTemplateId = params['assessmentTemplateId'];
        this.assessmentService.getAssessment(this.assessmentTemplateId).subscribe((data: any) => {
          this.assessmentTemplate = data;
          this.assessmentName = this.assessmentTemplate.assessmentName;
          this.json = JSON.parse(this.assessmentTemplate.questionJson);
          this.surveyItem = new Survey.Model(this.json);
        },
          err => console.error(err),
          () => this.renderDummyAssessment()
        );
      }
    });
  }

  renderDummyAssessment() {
    const defaultThemeColorsEditor = Survey
      .StylesManager
      .ThemeColors['default'];
    defaultThemeColorsEditor['$primary-color'] = '#2b769b';
    defaultThemeColorsEditor['$secondary-color'] = '#2b769b';
    defaultThemeColorsEditor['$main-color'] = '#2b769b';
    Survey.StylesManager.applyTheme();
    Survey.JsonObject.metaData.addProperty("itemvalue", { name: "score:number" });
    Survey.SurveyNG.render('surveyElement', {
      model: this.surveyItem
    });
  }

  verifySurveyResponse(result) {
    this.score = 0;
    if (!result.surveyItem.checkIsCurrentPageHasErrors()) {
      const scoreQuestion = result.surveyItem.getQuestionByName("score");
      if (scoreQuestion) {
        const keys = Object.keys(result.surveyItem.data);
        keys.forEach(questionName => {

          if (result.surveyItem.getQuestionByName(questionName) !== null) {
            const elementType = result.surveyItem.getQuestionByName(questionName).getType();
            if (elementType === 'checkbox') {
              const itemValue = result.surveyItem.data[questionName];
              itemValue.forEach(checkBoxOptions => {
                const count = this.getScore(questionName, checkBoxOptions);
                this.score = this.score + count;
              });
            } else if (elementType === 'bootstrapslider') {
              const itemValue = result.surveyItem.data[questionName];
              this.score = this.score + Number(itemValue);
            } else if (elementType !== 'expression' && elementType !== 'html' && elementType !== 'comment') {
              const itemValue = result.surveyItem.data[questionName];
              const count = this.getScore(questionName, itemValue);
              this.score = this.score + count;
            }
          }
        });
        scoreQuestion.value = this.score;
      }
    }
  }

  getScore(questionName, itemValue) {
    let count = 0;
    const keys = Object.keys(this.json);
    keys.forEach(element => {
      if ('pages' === element) {
        const pages = this.json.pages;
        pages.forEach(page => {
          count = this.getScoreByQuestion(page, questionName, itemValue);
        });

      }
    });
    return count;
  }

  getScoreByQuestion(page, questionName, itemValue) {
    let count = 0
    const elements = page.elements;
    elements.forEach(question => {
      if (question.name === questionName) {
        count = this.getCount(question, itemValue);
      }
    });

    return count;
  }

  getCount(question, itemValue) {
    let count = 0;
    if (question.rateValues && question.type === 'rating') {
      question.rateValues.forEach(choice => {
        if (choice.value === itemValue && choice.score) {
          count = Number(choice.score);
        }
      });
    } else {
      if (question.choices) {
        question.choices.forEach(choice => {
          if (choice.score && choice.value === itemValue) {
            count = Number(choice.score);
          }
        });
      }
    }
    return count;
  }
  
}
