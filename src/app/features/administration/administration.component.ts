import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdministrationNavigation } from './administration.nav';
import { InPageNavService } from './../shared/components/in-page-nav/in-page-nav.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnDestroy {

  constructor(
    private readonly inPageNavService: InPageNavService,
    private readonly administrationNavigation: AdministrationNavigation ) {
      this.inPageNavService.setNavItems(this.administrationNavigation.administrationPageMenu);
    }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

}