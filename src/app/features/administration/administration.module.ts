import { AdministrationNavigation } from './administration.nav';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdministrationComponent } from './administration.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministrationRoutingModule } from './administration-routing.module';
import { ContentImageModule } from './static-data/content-image/content-image.module';
import { MaterialModule } from '../../framework/material/material.module';
import { ContentCategoryModule } from './static-data/content-category/content-category.module';
import { ContentKeywordsModule } from './static-data/content-keywords/content-keywords.module';
import { SharedModule } from '../../framework/shared/shared.module';

@NgModule({
  declarations: [AdministrationComponent],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    ContentImageModule,
    ContentCategoryModule,
    ContentKeywordsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [AdministrationNavigation]
})
export class AdministrationModule { }