import { Component, OnInit } from '@angular/core';
import { SnackBarService } from '../../../framework/service/snack-bar.service';
import { TrackLiteSyncDataService } from './track-lite-sync-data.service';

@Component({
  selector: 'app-track-lite-sync-data',
  templateUrl: './track-lite-sync-data.component.html',
  styleUrls: ['./track-lite-sync-data.component.scss']
})
export class TrackLiteSyncDataComponent implements OnInit {

  constructor(
    private readonly trackLiteSyncDataService: TrackLiteSyncDataService,
    private readonly snackBarService: SnackBarService
  ) { }

  ngOnInit() {
  }

  syncData() {
    this.trackLiteSyncDataService.getAllTrackLiteSyncData().subscribe(response => {
      this.snackBarService.success(response.successMessage);
    }, error => {
      this.snackBarService.error(`${error.error.applicationMessage}`);
    });
  }

}
