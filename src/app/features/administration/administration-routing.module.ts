import { AuthorizationGuard } from './../../framework/guards/authorization.guard';
import { AuthenticationGuard } from './../../framework/guards/authentication.guard';
import { AdministrationComponent } from './administration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: AdministrationComponent,
    data: { title: 'Manage Roles', auth: [15] },
    children: [
      {
        path: 'content-sync-data', loadChildren: () => import('./sync-data/sync-data.module').then(m => m.SyncDataModule),
        data: { title: 'Content Sync Data', breadcrumb: '', preload: false, auth: [12] },
      },
      {
        path: 'track-lite-sync-data', loadChildren: () => import('./track-lite-sync-data/track-lite-sync-data.module').then(m => m.TrackLiteSyncDataModule),
        data: { title: 'Track Lite Sync Data', breadcrumb: '', preload: false, auth: [12] },
      },
      {
        path: 'content-category', loadChildren: () => import('./static-data/content-category/content-category.module').then(m => m.ContentCategoryModule),
        data: { title: 'Content Categories', breadcrumb: '', preload: false, auth: [11] },
        canActivate: [AuthenticationGuard, AuthorizationGuard]
      },
      {
        path: 'content-keywords', loadChildren: () => import('./static-data/content-keywords/content-keywords.module').then(m => m.ContentKeywordsModule),
        data: { title: 'Content Keywords', breadcrumb: '', preload: false, auth: [11] },
        canActivate: [AuthenticationGuard, AuthorizationGuard]
      },
      {
        path: 'content-image', loadChildren: () => import('./static-data/content-image/content-image.module').then(m => m.ContentImageModule),
        data: { title: 'Images', breadcrumb: '', preload: false, auth: [11] },
        canActivate: [AuthenticationGuard, AuthorizationGuard]
        
      }
    ],
  },

];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
