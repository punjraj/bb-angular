import { SnackBarService } from '../../../framework/service/snack-bar.service';
import { Component, OnInit } from '@angular/core';
import { SyncDataService } from './sync-data.service';


@Component({
  selector: 'app-sync-data',
  templateUrl: './sync-data.component.html',
  styleUrls: ['./sync-data.component.scss']
})
export class SyncDataComponent implements OnInit {

  constructor(private readonly syncDataService: SyncDataService,
    private readonly snackBarService: SnackBarService) { }

  ngOnInit() {
  }

  syncData() {
    this.syncDataService.getAllSyncData().subscribe(
      response => {
        this.snackBarService.success(response.applicationMessage);
      }, error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
    });
  }
}
