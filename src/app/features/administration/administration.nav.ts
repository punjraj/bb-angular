import { Injectable } from '@angular/core';
import { IInPageMenuItem } from './administration.interface';

const dataInputRoute = 'administration';

@Injectable()
export class AdministrationNavigation {

    administrationPageMenu = {

        sectionName: 'Administration',
        featureId: [15],
        description: ``,
        state: null,
        icon: 'person_outline',
        menuItems: [
            {
                name: 'Static Data',
                featureId: [11],
                description: ``,
                state: null,
                icon: null,
                submenu: [
                    {
                        name: 'Content Categories',
                        featureId: [13],
                        description: 'Details about Content Categories',
                        state: `${dataInputRoute}/content-category`,
                        icon: null
                    },
                    {
                        name: 'Content Keywords',
                        featureId: [13],
                        description: 'Details about Content Keywords',
                        state: `${dataInputRoute}/content-keywords`,
                        icon: null
                    },
                    {
                      name: 'Images',
                      featureId: [13],
                      description: 'Add Content Images',
                      state: `${dataInputRoute}/content-image`,
                      icon: null
                    }
                ]
            },
            {
                name: 'Sync Data',
                featureId: [12],
                description: ``,
                state: null,
                icon: null,
                submenu: [
                  {
                    name: 'Content Sync Data',
                    featureId: [12],
                    description: 'Content Sync Data',
                    state: `${dataInputRoute}/content-sync-data`,
                    icon: null
                  },
                  {
                    name: 'Track Lite Sync Data',
                    featureId: [13],
                    description: 'Track Lite Sync Data',
                    state: `${dataInputRoute}/track-lite-sync-data`,
                    icon: null
                },
                ]
            },
        ]
    };

}
