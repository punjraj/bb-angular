import { RoleComponent } from './role.component';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { ViewRoleComponent } from './view-role/view-role.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../../../../framework/guards/authentication.guard';
import { AuthorizationGuard } from '../../../../framework/guards/authorization.guard';

const routes: Routes = [
  { path: '',
    component: RoleComponent,
    data: { title: 'Manage Roles', auth: [10] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
   {
    path: 'edit-role/:id',
    component: EditRoleComponent,
    data: { title: 'Edit Role', auth: [10] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
  {
    path: 'add-role',
    component: EditRoleComponent,
    data: { title: 'Create Role', auth: [10] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
  {
    path: 'view-role/:id',
    component: ViewRoleComponent,
    data: { title: 'View Role', auth: [10] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
