export interface RoleDetails {
    roleName: string;
    featureNames: any;
    tenantName: string;
    actions: any;
}
