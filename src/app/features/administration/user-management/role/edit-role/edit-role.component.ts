import { RoleService } from './../role.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, FormArray } from '@angular/forms';
import { SnackBarService } from '../../../../../framework/service/snack-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss']
})
export class EditRoleComponent implements OnInit {
  tenantNameList: any[];
  roleDetailForm: FormGroup;
  isNew = false;
 
  constructor(
    private readonly fb: FormBuilder,
    private readonly roleService: RoleService,
    private readonly snackbar: SnackBarService,
    private readonly route: ActivatedRoute,
    private readonly router: Router) { }

  ngOnInit() {
    this.getTenantList();
    this.initDataForm();
  }

  initDataForm() {
    this.roleDetailForm = this.fb.group({
      id: [''],
      roleName: ['', [Validators.required, Validators.pattern('\\s*[A-Za-z0-9]+(?:\\s[A-Za-z0-9]+)*\\s*')
      ]],
      allowedMultipleEstablishments: [false],
      tenantId: ['', Validators.required],
    });

    this.route.params.subscribe((params: any) => {
      if (params.hasOwnProperty('id')) {
        const roleId = params['id'];
        this.roleService.getRoleById(roleId).subscribe((data: any) => {
          this.roleDetailForm.patchValue(data);
          this.roleDetailForm.controls.roleName.disable();
        }, error => {
          this.snackbar.error(`${error.error.applicationMessage}`);
          this.navigateHome();
        });
      } else {
        this.isNew = true;
      }
    });
  }

  onSubmit(data) {
    if(this.isNew) {
      this.createNewRole(data);
    } else {
      this.editRole(data);
    }
  }

  editRole(data) {
    this.roleService.updateRole(data.getRawValue()).subscribe(response => {
      this.snackbar.success(`Role ${response.roleName} has been updated.`);
      this.navigateHome();
    }, error => {
      this.snackbar.error(`${error.error.applicationMessage}`);
    });
  }


  getTenantList() {
    this.roleService.getTenants().subscribe(tenantList => {
      this.tenantNameList = tenantList;
    });
  }

  createNewRole(data) {
    this.roleService.saveRole(data.value).subscribe(response => {
      this.snackbar.success(`Role ${response.roleName} has been created`);
      this.navigateHome();
    }, error => {
      this.snackbar.error(`${error.error.applicationMessage}`);
    });
  }

  navigateHome() {
    this.router.navigate(['./administration/role'])
  }



}
