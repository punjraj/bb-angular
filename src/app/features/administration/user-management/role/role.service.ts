import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseUrl } from './../../../../framework/constants/url-constants';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(private readonly http: HttpClient) { }

  getAllRoles(sort: string, size: number, page: number, body: any): Observable<any> {
    const href = `${BaseUrl.AUTHORIZATION}/search`;
    return this.http.post<any>(href, body, {params: new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString())
      .set('sort', sort.toString())
    });
  }

  saveRole(payload) {
    const href = `${BaseUrl.AUTHORIZATION}/createRole`;
    return this.http.post<any>(href, payload);
  }
  
  getTenants() {
    const href = `${BaseUrl.AUTHORIZATION}/getTenantList`;
    return this.http.get<any>(href);
  }

  getRoleById(roleId) {
    const href = `${BaseUrl.AUTHORIZATION}/getRoleById/${roleId}`;
    return this.http.get<any>(href);
  }

  updateRole(payload) {
    const href = `${BaseUrl.AUTHORIZATION}/updateRole`;
    return this.http.put<any>(href, payload);
  }

  deleteRole(roleId) {
    const href = `${BaseUrl.AUTHORIZATION}/deleteByRole/${roleId}`;
    return this.http.delete<any>(href);
  }
}
