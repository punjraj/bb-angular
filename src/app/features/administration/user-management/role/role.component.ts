import { RoleDetails } from './role.interface';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { RoleService } from './role.service';
import { IUser } from './../../../manage-users/user.interface';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AppConfirmService } from '../../../../framework/components/app-confirm/app-confirm.service';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  displayedColumns: string[] = ['roleName', 'tenantName', 'featureNames', 'actions'];
  dataSource = new MatTableDataSource<RoleDetails>();

  
  sortColumn = 'roleName';
  sortDirection = 'asc';
  pageSize = 10;
  filterBy = { 'keyword': '' };

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private readonly roleService: RoleService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly appConfirmService: AppConfirmService,
  ) { }

  ngOnInit() {
    this.fetchRoleData(this.filterBy);
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(data => {
      if(data.active === 'tenantName') {
        this.sortColumn = 't.tenantName';
      } else {
        this.sortColumn = data.active;
      }
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
      this.fetchRoleData(this.filterBy);
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.fetchRoleData(this.filterBy);
          document.querySelector('#role').scrollIntoView();
        }
          )
      )
      .subscribe();
  }

  fetchRoleData(filterBy) {
    this.roleService.getAllRoles(`${this.sortColumn},${this.sortDirection}`, this.pageSize, this.paginator.pageIndex, filterBy).subscribe(
      data => {
        this.dataSource.data = data.content;
        this.paginator.length = data.totalElements;
      },
      error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
        this.router.navigate(['./administration/role']);
      }
    );
  }

  onFilter(filterValue: string) {
    this.filterBy.keyword = filterValue;
    this.paginator.pageIndex = 0;
    this.fetchRoleData(this.filterBy);
  }

  onEditClicked(e): void {
    this.router.navigate([`./edit-role/${e}`], { relativeTo: this.route });
  }

  onDeleteClicked(e,name) {
    const dialogRef = this.appConfirmService.confirm({
      title: `Delete User`,
      message: `Are you sure you want to delete role - ${name}?`,
    });
    
    dialogRef.subscribe(result => {
      if (result) {
        this.roleService.deleteRole(e).subscribe(
            response => {
              this.snackBarService.success(`The role has been deleted`);
              this.fetchRoleData(this.filterBy);
            },
            error => this.snackBarService.error(`${error.error.applicationMessage}`)
          );
      }
    });
  }

}
