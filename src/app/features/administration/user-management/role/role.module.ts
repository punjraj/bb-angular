import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { ViewRoleComponent } from './view-role/view-role.component';
import { AppConfirmService } from '../../../../framework/components/app-confirm/app-confirm.service';
import { AppConfirmModule } from '../../../../framework/components/app-confirm/app-confirm.module';
import { RoleRoutingModule } from './role-routing.module';
import { RoleComponent } from './role.component';
import { MaterialModule } from '../../../../framework/material/material.module';

@NgModule({
  declarations: [EditRoleComponent, ViewRoleComponent, RoleComponent],
  imports: [
    CommonModule,
    RoleRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    AppConfirmModule
  ],
  providers: [AppConfirmService]
})
export class RoleModule { }
