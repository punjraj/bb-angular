import { Router, ActivatedRoute } from '@angular/router';
import { RoleService } from './../role.service';
import { Component, OnInit } from '@angular/core';
import { SnackBarService } from '../../../../../framework/service/snack-bar.service';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.scss']
})
export class ViewRoleComponent implements OnInit {
  role: any = {};

  constructor(
    private readonly roleService: RoleService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
  ) { }

  ngOnInit() {
    this.viewRolebyId();
  }

  viewRolebyId() {
    this.route.params.subscribe((params: any) => {
      if (params.hasOwnProperty('id')) {
        this.roleService.getRoleById(+params['id']).subscribe(data => {
          this.role = data;
        },
          error => {
            this.snackBarService.error(`${error.error.applicationMessage}`);
            this.router.navigate(['./administration/role']);
          }
        );
      } 
    });

  }
  onEditClicked() {
    this.router.navigate([`./administration/role/edit-role/${this.role.id}`]);
  }
}
