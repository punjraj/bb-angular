import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentKeywordsComponent } from './content-keywords.component';
import { AuthenticationGuard } from '../../../../framework/guards/authentication.guard';
import { AuthorizationGuard } from '../../../../framework/guards/authorization.guard';
import { EditKeywordComponent } from './edit-keyword/edit-keyword.component';

const routes: Routes = [
  { path: '',
    component: ContentKeywordsComponent,
    data: { title: 'Manage Content Keywords', auth: [11,2] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
  {
    path: 'add-keyword',
    component: EditKeywordComponent,
    data: { title: 'Add Content Keywords', auth: [11,1] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentKeywordsRoutingModule { }
