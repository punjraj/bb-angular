import { AppConfirmService } from './../../../../framework/components/app-confirm/app-confirm.service';
import { IKeyword } from '../../../../features/content-management/content.interface';
import { Component, OnInit, ViewChild, AfterViewInit, Input } from '@angular/core';
import { Keywords } from './content-keywords.interface';
import { ContentKeywordsService } from './content-keywords.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { SessionsService } from '../../../../sessions/sessions.service';



@Component({
  selector: 'app-content-keywords',
  templateUrl: './content-keywords.component.html',
  styleUrls: ['./content-keywords.component.scss']
})
export class ContentKeywordsComponent implements OnInit {
  dataSource = new MatTableDataSource<Keywords>();
  public keywordOptions: IKeyword[];
  public size: number = 5000;
  public page: number = 0;
  filterBy = { 'searchString': '', keywordType: 'Content' };
  deleteKeywordAuth : string [] = ['11','4'];
  isRemovable:boolean = true;

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(
    private readonly keywordService: ContentKeywordsService,
    private readonly router: Router,
    private readonly snackBarService: SnackBarService,
    private readonly appConfirmService: AppConfirmService,
    private readonly sessionService:SessionsService
  ) { }

  ngOnInit() {
    this.fetchKeywordData(this.filterBy);
    this.isRemovableKeyword();
  }

  isRemovableKeyword(){
    if(!this.sessionService.hasResource(this.deleteKeywordAuth)){
      this.isRemovable = false;
    }
  }

  fetchKeywordData(filterBy) {
    this.keywordService
    .getAllKeywords( this.size, this.page,filterBy).subscribe(
      data => {
        this.keywordOptions = data.content;
       },
      error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
        this.router.navigate(['./administration/content-keywords']);
      }
    );
  }

  onFilter(value) {
    this.filterBy.searchString = value;
    this.fetchKeywordData(this.filterBy);
  }

  removeKeyword(elementId: number): void {

    const dialogRef = this.appConfirmService.confirm({
      title: `Delete keyword`,
      message: `Are you sure you want to delete keyword, this may be linked to active content?`,
      showTextField: false,
      placeholderTextField: ''
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.keywordService
          .deleteKeyword({ id: elementId }).subscribe(
            (response: any) => {
              this.snackBarService.success(response.applicationMessage);
              this.fetchKeywordData(this.filterBy);
            },
            error => this.snackBarService.error(`${error.error.applicationMessage}`)
          );
      }
    });

 
  }

}
