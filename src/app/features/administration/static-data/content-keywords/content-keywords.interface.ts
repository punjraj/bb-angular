export interface Keywords {
    id: number;
    keywordName: string;
    keywordType: string;
    actions: any;
}
