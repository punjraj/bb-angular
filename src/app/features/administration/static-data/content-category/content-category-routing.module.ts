import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentCategoryComponent } from './content-category.component';
import { AuthenticationGuard } from '../../../../framework/guards/authentication.guard';
import { AuthorizationGuard } from '../../../../framework/guards/authorization.guard';
import { EditContentCategoryComponent } from './edit-content-category/edit-content-category.component';

const routes: Routes = [
  { path: '',
    component: ContentCategoryComponent,
    data: { title: 'Manage Content Categories', auth: [11] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
  { path: 'new',
    component: EditContentCategoryComponent,
    data: { title: 'Add Content Category', auth: [11, 1] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
  { path: 'edit/:id',
    component: EditContentCategoryComponent,
    data: { title: 'Edit Content Category', auth: [11, 3] },
    canActivate: [AuthenticationGuard, AuthorizationGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentCategoryRoutingModule { }
