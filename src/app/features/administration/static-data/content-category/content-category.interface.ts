export interface CategoryDetails {
    categoryName: string;
    menuLevel: string;
    actions: any;
}
