import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrl } from '../../framework/constants/url-constants';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private readonly reportType = 'FAB_DAILY_ASSESSMENT_RESPONSE_REPORT_V2';
  private readonly client = 'BOUNCEBACK';

  constructor(private readonly http: HttpClient) {
  }

  extractUsersLLIFdata(fileName: string) {
    const href = `${BaseUrl.USER}/report/user-llif/1`;
    return this.http.get<Blob>(href, { params: { 'fname': fileName }, observe: 'response', responseType: 'blob' as 'json' });
  }

  extractPlanLLIFdata(fileName: string, extractType: string) {
    const href = `${BaseUrl.PLAN}/extract-llif/` + extractType;
    return this.http.get<Blob>(href, { params: { 'fname': fileName }, observe: 'response', responseType: 'blob' as 'json' });
  }

  getAvailableReports() {
    return this.http.get(`${BaseUrl.REPORT}/report/fab/bounceback/report-types`);
  }


  fabReport(fileName: string, extractType: string) {
    const href = `${BaseUrl.REPORT}/report`;
    return this.http.get<Blob>(href, {
      params: {
        'client': this.client
        , 'report': this.reportType
        , 'name': fileName
      }, observe: 'response', responseType: 'blob' as 'json'
    });
  }
}
