import { Component, OnInit } from '@angular/core';
import { ReportsService } from './reports.service';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  reportsExtractsForm: FormGroup;
  staticExtractTypes = [
    { id: 1, reportName: 'Service User Info', reportDescription: 'Service User Info', fileName: 'Service User Info' },
    { id: 2, reportName: 'Interventions', reportDescription: 'Interventions', fileName: 'Interventions' },
    { id: 3, reportName: 'Appointments', reportDescription: 'Appointments', fileName: 'Appointments' },

  ];
  constructor(
    private readonly reportService: ReportsService,
    private readonly snackBarService: SnackBarService,
    private readonly fb: FormBuilder) {
  }

  ngOnInit() {

    this.reportsExtractsForm = this.fb.group({
      extractType: [null, Validators.required],
    });

    this.reportService.getAvailableReports().subscribe(rsp => {
      Object.values(rsp).forEach(r => {
        this.staticExtractTypes.push({ id: this.staticExtractTypes.length + 1, reportName: r, reportDescription: r, fileName: r });
      })

    })
  }


  onSubmit(reportsExtractsForm) {
    var { extractRsp, fileName }: { extractRsp: any; fileName: string; }
      = this.extractReportsByType(reportsExtractsForm);

    extractRsp.subscribe(
      response => {
        if (response.status === 204) {
          this.snackBarService.error("Report not found");
        } else {
          let binaryData = [];
          binaryData.push(response.body);
          let responseFileName: string = this.getResponseFileName(response, fileName);
          let extractLink = document.createElement('a');
          extractLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: response.headers.get('Content-Type') }));
          extractLink.setAttribute('download', responseFileName);
          document.body.appendChild(extractLink);
          extractLink.click();
          extractLink.remove();
        }
      },
      error => {
        this.snackBarService.error(error.error.applicationMessage);
      }
    );
  }

  private extractReportsByType(reportsExtractsForm: any) {
    let extractRsp: any;
    const type = reportsExtractsForm.get('extractType').value;
    const fileName = this.staticExtractTypes.filter(et => et.reportName === type)[0].fileName;
    if (type === 'Service User Info') {
      extractRsp = this.reportService.extractUsersLLIFdata(fileName);
    } else if (type === 'Interventions' || type === 'Appointments') {
      extractRsp = this.reportService.extractPlanLLIFdata(fileName, type.toLowerCase());
    } else {
      extractRsp = this.reportService.fabReport(fileName, type.toLowerCase());
    }
    return { extractRsp, fileName };
  }

  getResponseFileName(response: HttpResponse<Blob>, defaultName: string) {
    let fileName: string;
    try {
      const contentDisposition: string = response.headers.get('Content-Disposition');
      const pattern = /(?:filename="?)(.+)(?:"?)/
      fileName = pattern.exec(contentDisposition)[1];
    }
    catch (err) {
      fileName = defaultName + '.csv';
    }
    return fileName
  }

}
