import { Injectable } from '@angular/core';

const dataInputRoute = 'job-advert';

@Injectable()
export class JobAdvertsNavigation {

    jobAdvertPageMenu = {

        sectionName: 'Job Adverts',
        featureId: [17],
        description: ``,
        state: null,
        icon: 'person_outline',
        menuItems: [
            {
                name: 'Local Jobs',
                featureId: [19],
                description: `Local Jobs`,
                state: `${dataInputRoute}/local-jobs`,
                icon: null,
                submenu: [
                ]
            }
        ]
    };

}
