import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobAdvertsRoutingModule } from './job-adverts-routing.module';
import { JobAdvertsNavigation } from './job-adverts.nav';
import { JobAdvertsComponent } from './job-adverts.component';
import { MaterialModule } from '../../framework/material/material.module';

@NgModule({
  declarations: [JobAdvertsComponent],
  imports: [
    CommonModule,
    JobAdvertsRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  providers: [JobAdvertsNavigation]
})
export class JobAdvertsModule { }
