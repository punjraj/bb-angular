
import { Routes } from '@angular/router';
import { JobAdvertsComponent } from './job-adverts.component';

export const JobAdvertsRoutes: Routes = [
  {
    path: '',
    component: JobAdvertsComponent,
    data: { title: 'Job Adverts', auth: [17,4] },
    children: [
      {
        path: 'local-jobs',
        loadChildren: () => import('./local-jobs/local-jobs.module').then(m=>
          m.LocalJobsModule),
        data: { title: 'Local Jobs', breadcrumb: '', preload: false, auth: [19,4] },
      }
    ],
  }
];
