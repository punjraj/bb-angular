import { Component, OnInit, Input } from '@angular/core';
import { LocalJobsService } from '../local-jobs.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppConfirmService } from '../../../../framework/components/app-confirm/app-confirm.service';
import { Location } from '@angular/common';
import { ServiceUserNavigation } from '../../../service-user/service-user-nav';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { InPageNavService } from '../../../shared/components/in-page-nav/in-page-nav.service';

@Component({
  selector: 'su-view-local-jobs',
  templateUrl: './view-local-jobs.component.html',
  styleUrls: ['./view-local-jobs.component.scss']
})
export class ViewNfnJobsComponent implements OnInit {
  jobData: any = {};
  SU : any;
  @Input() jobId: any;
  flag: any =true;
  userId : any = 0;
  constructor(
    private readonly localJobService: LocalJobsService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly location: Location,
    private readonly appConfirmService: AppConfirmService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly inPageNavService: InPageNavService,
  ) {
    this.route.params.subscribe(params => {
      if (params.hasOwnProperty('jobId')) {
        this.jobId = params.jobId;
      }
    });
    this.route.queryParams.subscribe((params: any) => {
      if(params.hasOwnProperty('id')){
        this.SU = params.SU;
        this.userId = params.id;
        this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
      }
    });
  }

  ngOnInit() {
    this.localJobService.getLocalJobForSU(this.jobId,this.userId).subscribe(data => {
      this.jobData = data;
    });
  }
  onExitClicked() {
    this.location.back();
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }
}
