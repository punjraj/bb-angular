
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ServiceUserComponent } from "./service-user.component";
import { EditServiceUserComponent } from "./edit-service-user/edit-service-user.component";
import { ViewUserProfileComponent } from "./view-service-user/view-user-profile/view-user-profile.component";
import { CompleteAssessmentsComponent } from "./complete-assessments/complete-assessments.component";
import { CompleteAssessmentComponent } from "./complete-assessments/complete-assessment/complete-assessment.component";
import { ViewCompleteAssessmentComponent } from "./complete-assessments/view-assessment/view-assessment.component";
import { ViewUserCvComponent } from './view-service-user/view-user-cv/view-user-cv.component';
import { DocumentsComponent } from './documents/documents.component';
import { UploadDocumentComponent } from './documents/upload-document/upload-document.component';
import { NgHttpLoaderModule } from 'ng-http-loader';
import { MeetingListComponent } from './meet/meeting-list/meeting-list.component';
import { MeetingVideoComponent } from './meet/meeting-video/meeting-video.component';
import { MeetComponent } from './meet/meet.component';
import { CanDeactivateGuard } from '../../framework/guards/can-deactivate/can-deactivate.guard';
import { AuthorizationGuard } from '../../framework/guards/authorization.guard';
import { AuthenticationGuard } from '../../framework/guards/authentication.guard';
import { FavouritesJobsComponent } from './job-activity/favourites-jobs/favourites-jobs.component';
import { ViewFavouritesJobsComponent } from './job-activity/view-favourites-job/view-favourites-jobs/view-favourites-jobs.component';
import { JobApplicationsComponent } from './job-activity/job-applications/job-applications.component';
import { JobActivityComponent } from './job-activity/job-activity.component';
import { LocalJobsComponent } from './job-activity/local-jobs/local-jobs.component';
import { CompleteAssessmentsTabsComponent } from './complete-assessments/complete-assessments-tabs/complete-assessments-tabs.component';
import { PlanInductionComponent } from "./view-service-user/plan-induction/plan-induction.component";
import { PlanContentCardComponent } from "./view-service-user/plan-content-card/plan-content-card.component";
import { ProfileTabsComponent } from './view-service-user/profile-tabs/profile-tabs.component';

const localjobs = 'local-jobs';
const routes: Routes = [
  {
    path: "",

    component: ServiceUserComponent,
    data: { title: 'Participant' , auth: [9] },
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: "new",
    component: EditServiceUserComponent,
    data: { title: "New Participant", auth: [9, 1] }
  },
  {
    path: 'edit/:id',
    component: EditServiceUserComponent,
    data: { title: 'Edit Participant', auth: [9, 3]}
  },
  {
    path: 'profile-tabs',
    component: ProfileTabsComponent,
    children: [
      {
        path: '',
        redirectTo: 'view-user-profile',
        pathMatch: 'full',
      },
      {
        path: 'view-user-profile',
        component: ViewUserProfileComponent,
      },
      {
        path: "new-assessment", loadChildren: () => import('../service-user/view-service-user/assessment/assessment.module').then(
          m => m.AssessmentModule
        ),
        },
    ]
  },
  {
    path: 'plan',
    component: PlanContentCardComponent,
  },
  {
    path: 'assessments',
    component: CompleteAssessmentsTabsComponent,
    children: [
      {
        path: '',
        redirectTo: 'todo',
        pathMatch: 'full'
      },
      {
        path: ":status",
        component: CompleteAssessmentsComponent,
      },
      {
        path: ":status/edit",
        component: CompleteAssessmentComponent,
      },
      {
        path: ":status/view",
        component: ViewCompleteAssessmentComponent,
      },
    ]
  },
  {
    path: "cv",
    component: ViewUserCvComponent,
  },
  {
    path: 'documents',
    component: DocumentsComponent,
    data: { title: 'Documents', auth: [9] },
    canActivate: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'documents/upload',
    component: UploadDocumentComponent,
    data: { title: 'Upload Documents', auth: [9] },
    canActivateChild: [AuthenticationGuard, AuthorizationGuard],
  },
  {
    path: 'meet',
    component: MeetComponent,
    children: [
      {
        path: '', component: MeetingListComponent,
        data: { title: 'Meetings', auth: [20] },
        canActivateChild: [AuthenticationGuard, AuthorizationGuard]
      },
      {
        path: 'join',
        component: MeetingVideoComponent,
        data: { auth: [9] },
        canActivateChild: [AuthenticationGuard, AuthorizationGuard],
        canDeactivate: [CanDeactivateGuard]
      },
    ]
  },
   {
    path: 'job-activity',
    component: JobActivityComponent,

    children: [
      {
        path: '',
        redirectTo: localjobs,
        pathMatch: 'full',
      },
      {
        path: localjobs,
        component: LocalJobsComponent,
        data: { auth: [19] },
        canActivateChild: [AuthenticationGuard, AuthorizationGuard],
      },
      {
        path: 'job-applications',
        loadChildren: () => import('../application/application.module').then(
          m => m.ApplicationModule
        ),
        data: { preload: false, auth: [25] }
      },
      {
        path: 'local-jobs',
        loadChildren: () => import('../job-adverts/local-jobs/local-jobs.module').then(
          m => m.LocalJobsModule
        ),
        data: { preload: false, auth: [19, 4] },
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServiceUserRoutingModule { }
