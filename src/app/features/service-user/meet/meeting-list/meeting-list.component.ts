import { Component, OnInit } from '@angular/core';
import { InPageNavService } from '../../../../features/shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../../service-user-nav';
import { MeetService } from '../meet.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ServiceUserService } from '../../service-user.service';

@Component({
  selector: 'app-meeting-list',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.scss']
})
export class MeetingListComponent implements OnInit {

  serviceUserId;
  fullName: string;
  meetings: any[] = [];
  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly meetService: MeetService,
    private readonly serviceUserService: ServiceUserService,
    private readonly builder: FormBuilder,
  ) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
  }

  ngOnInit() {

  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      this.route.snapshot.data['title'] = params.fullName;
      this.serviceUserId = params.id;
      this.fullName = params.fullName;
    });
  }

  startSession() {

    const data = {serviceUserId : this.serviceUserId , suFullname: this.fullName};
    const tokenBody = atob(localStorage.getItem('token').split('.')[1]);
    const payload = JSON.parse(tokenBody);
    this.serviceUserService.sendNotification({sender : payload.userId, receiver: this.serviceUserId, subscriptionType: 'CALL',
    callNotificationType: 'INCOMING_CALL', senderName: payload.fullName}).subscribe();
    this.meetService.createSession(data).subscribe(rsp => {
        this.router.navigate(['join'], { relativeTo: this.route, queryParamsHandling: 'merge'});
    });
  }

  onExitClicked() {

  }

}
