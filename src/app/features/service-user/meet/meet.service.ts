import { Injectable } from '@angular/core';
import { BaseUrl } from '../../../framework/constants/url-constants';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetService {
  
  constructor(private readonly http: HttpClient) { }
  
  createSession(body) {
    const href = `${BaseUrl.OPENVIDU}/sessions`;
    return this.http.post(href, body);
  }
  
  stopSession(body) {
    const href = `${BaseUrl.OPENVIDU}/sessions/stop`;
    return this.http.post(href, body);
  }
  
  getMeetings() {
    const href = `${BaseUrl.OPENVIDU}/lessons`;
    return this.http.get(href);
  }
  
  generateToken(body) {
    const href = `${BaseUrl.OPENVIDU}/sessions/generate-token`;
    return this.http.post(href, body);
  }
  
  startRecording(body) {
    const href = `${BaseUrl.OPENVIDU}/recordings/start`;
    return this.http.post(href, body);
  }
  
  stopRecording(body) {
    const href = `${BaseUrl.OPENVIDU}/recordings/stop`;
    return this.http.post(href, body);
  }
  
  getRecordingList(body) {
    const href = `${BaseUrl.OPENVIDU}/recordings/list`;
    return this.http.post(href, body);
  }
  

  findAllRecordings(sort: string, page: number, size: number, body: any): Observable<any> {
    const href = `${BaseUrl.OPENVIDU}/recordings/list`;
    return this.http.post<any>(href, body, {
      params: new HttpParams()
        .set('sort', sort.toString())
        .set('page', page.toString())
        .set('size', size.toString())
    });
  }

  downloadRecording(recordingId, sessionId) {
    const href = `${BaseUrl.OPENVIDU}/recordings/getRecording/${recordingId}/${sessionId}`;
    return this.http.get(href, { responseType: 'blob' as 'text', observe: 'response' });
  }
  
  getUrl(recordingId) {
    return `${BaseUrl.OPENVIDU}/recordings/getRecording/${recordingId}/1/stream?access_token=Bearer ${localStorage.getItem('token')}`;
  }

}
