import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MeetingVideoComponent } from './meeting-video.component';

describe('MeetingVideoComponent', () => {
  let component: MeetingVideoComponent;
  let fixture: ComponentFixture<MeetingVideoComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
