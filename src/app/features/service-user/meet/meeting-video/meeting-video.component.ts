import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InPageNavService } from '../../../../features/shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../../service-user-nav';
import { MeetService } from '../meet.service';
import { OpenVidu, Session, Publisher, StreamEvent, ConnectionEvent } from 'openvidu-browser';
import { AppConfirmService } from '../../../../framework/components/app-confirm/app-confirm.service';
import { BaseUrl } from '../../../../framework/constants/url-constants';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { ServiceUserService } from '../../service-user.service';
import { ManageUsersService } from '../../../../features/manage-users/manage-users.service';
import { Utility } from '../../../../framework/utils/utility';

@Component({
  selector: 'app-meeting-video',
  templateUrl: './meeting-video.component.html',
  styleUrls: ['./meeting-video.component.scss']
})
export class MeetingVideoComponent implements OnInit, OnDestroy {

  publishers: any[] = [];
  meetingId;
  OV: OpenVidu;
  session: Session;
  publisher: Publisher;
  serviceUserId;
  isRecording: boolean = false;
  source: EventSource;
  serviceUserFullName: string;
  serviceUserInitials: string;
  facilitatorFullName: string;
  facilitatorInitials: string;
  facilitatorRole;
  hideInviteMessage = false;
  callDuration: number;
  formattedCallDuration: any;
  recordingId;

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly snackBarService: SnackBarService,
    private readonly meetService: MeetService,
    private readonly serviceUserService: ServiceUserService,
    private readonly manageUserService: ManageUsersService,
    private readonly confirmService: AppConfirmService,
    private readonly activatedRoute: ActivatedRoute
  ) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
    this.setServiceUserFullName();
    this.setFacilitatorFullName();
  }

  ngOnInit() {

    this.route.queryParams.subscribe((params: any) => {
      this.serviceUserId = params.id;
    });
    const token = localStorage.getItem('token');
    const payload = JSON.parse(atob(token.split('.')[1]));
    if (payload.roleId === -1) {
      this.facilitatorRole = 'Super User';
    } else {
      this.manageUserService.getRoles().subscribe(roles => {
        const role: any = Utility.getObjectFromArrayByKeyAndValue(roles, 'roleId', payload.roleId);
        if (role) {
          this.facilitatorRole = role.roleName;
        }
      })
    }
    this.source = new EventSource(`${BaseUrl.USER}/call/${payload.userId}/CALL_RESPONSE/subscribe?access_token=Bearer ${token}`);
    this.source.addEventListener('CALL_RESPONSE', (message: MessageEvent) => {
      const notification = JSON.parse(message.data);
      if (notification.callNotificationType === 'CALL_DECLINED') {
        this.snackBarService.error('Service user declined the call');
        this.endSession();
      }
      if (notification.callNotificationType === 'CALL_ACCEPTED') {
        this.startTimer();
        this.serviceUserService.broadcast({
          sender: notification.sender,
          callNotificationType: 'IN_CALL', subscriptionType: 'SESSION'
        }).subscribe();
      }
    });
    const data = { serviceUserId: this.serviceUserId }
    this.meetService.generateToken(data).subscribe((rsp: any) => {
      this.OV = new OpenVidu();
      this.session = this.OV.initSession();
      this.session.on('streamCreated', (event: StreamEvent) => {
        this.session.subscribe(event.stream, 'subscriber', {
          insertMode: 'APPEND',
          subscribeToVideo: true,
          subscribeToAudio: true
        });
      });
      this.session.on('connectionCreated', (event: ConnectionEvent) => {
        this.publishers.push(event.connection.connectionId);
      });
      this.session.connect(rsp.token).then(() => {
        this.publisher = this.OV.initPublisher('publisher', {
          audioSource: undefined,
          videoSource: undefined,
          publishAudio: true,
          publishVideo: true,
          resolution: '1280x720',
          frameRate: 30,
          insertMode: 'APPEND',
          mirror: false
        });
        this.session.publish(this.publisher);
      });
    });
  }

  setServiceUserFullName() {
    this.activatedRoute.queryParams.subscribe(params => {
      const fullName = params['fullName'];
      this.serviceUserFullName = fullName;
      const nameSplitArr = this.serviceUserFullName.split(' ');
      this.serviceUserInitials = `${nameSplitArr[0][0]}${nameSplitArr[1][0]}`
    });
  }

  setFacilitatorFullName() {
    const tokenBody = atob(localStorage.getItem('token').split('.')[1]);
    const payload = JSON.parse(tokenBody);
    this.facilitatorFullName = payload.fullName;
    const nameSplitArr = this.facilitatorFullName.split(' ');
    this.facilitatorInitials = `${nameSplitArr[0][0]}${nameSplitArr[1][0]}`
  }

  endSession() {
    const sessionName = '';
    if (this.isRecording) {
      const dialogRef = this.confirmService.confirm({
        message: `Are you sure you want to stop Session ? `,
        showTextField: true,
        placeholderTextField: 'Session Title'
      });
      dialogRef.subscribe(result => {
        if (result) {
          this.destroySession(result);
        }
      });
    } else {
      this.destroySession(sessionName);
    }
    const body = { receiver: this.serviceUserId, subscriptionType: 'CALL', callNotificationType: 'CALL_HANGOUT' };
    this.serviceUserService.sendNotification(body).subscribe();
  }

  destroySession(SName) {
    this.hideInviteMessage = true;
    this.meetService.stopSession({ sessionId: this.session.sessionId, sessionName: SName, recordingId: this.recordingId }).subscribe(rsp => {
      this.session.disconnect();
      this.session = null;
      this.router.navigate(['/service-user']);
    });
  }

  stopActiveSession(result) {
    this.meetService.stopSession({ sessionId: this.session.sessionId, sessionName: result, recordingId: this.recordingId }).subscribe(rsp => {
      const body = { receiver: this.serviceUserId, subscriptionType: 'CALL', callNotificationType: 'CALL_HANGOUT' };
      this.serviceUserService.sendNotification(body).subscribe();
      this.session.disconnect();
      this.session = null;
    });
  }

  startRecording() {
    const kwId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userId;
    this.meetService.startRecording({
      sessionId: this.session.sessionId,
      lessonId: this.meetingId, keyworkerId: kwId, name: 'Meeting session 1'
    }).subscribe((rsp: any) => {
      this.isRecording = true;
      this.recordingId = rsp.recordingId;
    })
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      this.route.snapshot.data.title = params.fullName;
      this.meetingId = params.meetingId;
    });
  }

  canDeactivate() {
    if (this.session) {
      const dialogRef = this.confirmService.confirm({
        message: `Navigating from the ongoing session will end the video call. Do you still want to continue?`,
        showTextField: false,
        placeholderTextField: 'Session Title'
      });
      return dialogRef;
    }
    return true;
  }
  ngOnDestroy() {
    if (this.session) {
      this.stopActiveSession('');
    }
    const token = localStorage.getItem('token');
    const payload = JSON.parse(atob(token.split('.')[1]));
    this.serviceUserService.unsubscribe(payload.userId, 'CALL_RESPONSE').subscribe();
    this.source.close();
  }

  startTimer() {
    const start = Date.now();
    setInterval(() => {
      const delta = Date.now() - start;
      this.callDuration = Math.floor(delta / 1000);
      const date = new Date(null);
      date.setSeconds(this.callDuration)
      this.formattedCallDuration = date.toISOString().substr(11, 8);
    }, 1000);
  }
}
