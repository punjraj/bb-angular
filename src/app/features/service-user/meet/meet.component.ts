import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InPageNavService } from '../../shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../service-user-nav';

@Component({
  selector: 'app-meet',
  templateUrl: './meet.component.html',
  styleUrls: ['./meet.component.scss']
})
export class MeetComponent implements OnInit  {

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
  ) {
    this.setTitle();
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
  }

  ngOnInit() {
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      this.route.snapshot.data.title = params.fullName;
    });
  }

}
