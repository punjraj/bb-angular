import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Location } from '@angular/common';
import { tap } from 'rxjs/operators';

import { ServiceUserNavigation } from '../service-user-nav';
import { InPageNavService } from '../../shared/components/in-page-nav/in-page-nav.service';
import { DocumentsService } from './documents.service';
import { SnackBarService } from '../../../framework/service/snack-bar.service';
import { AppConfirmService } from '../../../framework/components/app-confirm/app-confirm.service';


@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit, OnDestroy {

  fname: string;
  lname: string;
  prn: string;
  userId: number;

  columns: string[] = ['name', 'createdDate', 'description', 'actions'];
  dataSource = new MatTableDataSource<any>();
  pageSize = 10;
  deleteReasons: any[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly router: Router,
    private readonly location: Location,
    private readonly route: ActivatedRoute,
    private readonly pageNav: InPageNavService,
    private readonly docService: DocumentsService,
    private readonly serviceUserNav: ServiceUserNavigation,
    private readonly snack: SnackBarService,
    private readonly confService: AppConfirmService,
  ) {
    this.pageNav.setNavItems(this.serviceUserNav.serviceUserSubMenu);
    this.setTitle();
  }

  ngOnInit() {
    this.fetchDeleteReasons();
    this.fetchDocuments(0, 0);


  }

  ngOnDestroy() {
    this.pageNav.setNavItems(null);
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      this.fname = params.fullName;
      this.userId = params.id;
      this.route.snapshot.data.title = `${this.fname}`;
    });
  }

  deleteDoc(doc: any) {
    const dialogRef = this.confService.confirm({
      title: `Delete Document`,
      message: `Please select a reason for deleting document`,
      showTextField: false,
      placeholderTextField: '',
      showSelectField: true,
      placeholderSelectField: `Please select`,
      optionsSelectField: this.deleteReasons
    });

    dialogRef.subscribe(result => {
      if (result) {
        this.docService.deleteDocument(doc.id, result)
          .subscribe(res => {
            this.snack.success(`The document is deleted`);
            this.paginator.firstPage();
            this.fetchDocuments(this.paginator.pageSize, this.paginator.pageIndex);
          }, error => {
            this.snack.error(`${error.error.applicationMessage}`);
          });
      }
    });
  }

  exit() {
    this.location.back();
  }

  private fetchDocuments(pageSize: number, pageIndex: number) {
    this.docService.listOfDocuments(this.userId, pageSize, pageIndex)
      .subscribe(data => {
        this.dataSource.data = data.content;
        this.dataSource.paginator = this.paginator;
      });
  }

  private fetchDeleteReasons() {
    this.docService.fetchDeleteReasons().subscribe(data => {
      this.deleteReasons = data.map(each => {
        return {
          id: each.id,
          reason: each.description
        }
      });
    });
  }

  download(id) {
    this.docService.download(id).subscribe(res => {
      console.log("done");
    });
  }

}
