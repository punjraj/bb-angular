import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceUserService } from './service-user.service';
import { tap } from 'rxjs/internal/operators/tap';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { InPageNavService } from '../shared/components/in-page-nav/in-page-nav.service';
import { MeetService } from './meet/meet.service';
import { BaseUrl } from '../../framework/constants/url-constants';
import { ApplicationConstant } from '../../framework/constants/app-constant';

@Component({
  selector: 'app-service-user',
  templateUrl: './service-user.component.html',
  styleUrls: ['./service-user.component.scss']
})

export class ServiceUserComponent implements OnInit, AfterViewInit, OnDestroy {

  displayedColumns: string[] = ['fullName', 'dateOfBirth', 'prn', 'project', 'accountState' , 'actions'];
  dataSource = new MatTableDataSource<any>();

  sortColumn = 'fullName';
  sortDirection = 'asc';
  pageSize = 10;
  filterBy = { 'keyword': '' , 'refUserType':'SU'};
  source: EventSource;
  loggedInUserId;
  SU: string = 'SU';
  constructor(
    private readonly serviceUserService: ServiceUserService,
    private readonly meetService: MeetService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly inPageNavService: InPageNavService
  ) {
    this.inPageNavService.setNavItems(null);
  }

  @ViewChild(MatPaginator , {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort , {static:false}) sort: MatSort;
  filterArray = [];
  filterHeader = 'Project';
  ngOnInit() {
    this.resolveUsers(this.filterBy);
    this.fetchProjectList(this.filterBy);
    const token = localStorage.getItem('token');
    this.loggedInUserId = JSON.parse(atob(token.split('.')[1])).userId;
    this.source = new EventSource(`${BaseUrl.USER}/call/${this.loggedInUserId}/SESSION/subscribe?access_token=Bearer ${token}`);
    this.source.addEventListener('SESSION', (message: MessageEvent) => {
      const notification = JSON.parse(message.data);
      const su = this.dataSource.data.filter(o => o.id === parseInt(notification.sender));
      if(su.length === 1) {
        su[0].callStatus = notification.callNotificationType;
      }
    });
  }

  ngAfterViewInit() {

    this.sort.sortChange.subscribe(data => {
      this.sortColumn = data.active;
      this.sortDirection = data.direction;
      this.paginator.pageIndex = 0;
      this.resolveUsers(this.filterBy);
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.resolveUsers(this.filterBy);
          document.querySelector('#service-users').scrollIntoView();
        }
        )
      )
      .subscribe();
  }

  resolveUsers(filterBy) {
    let currentPageIndex = 0;
    if (!this.paginator) {
      currentPageIndex = 0;
    } else {
      currentPageIndex = this.paginator.pageIndex;
    }
    this.serviceUserService
      .getServiceUsers(`${this.sortColumn},${this.sortDirection}`, this.pageSize, currentPageIndex, filterBy)
      .subscribe(data => {
        this.dataSource.data = data.content;
        this.paginator.length = data.totalElements;
      },
        error => {
          this.snackBarService.error(`${error.error.applicationMessage}`);
          this.router.navigate(['./service-user']);
        }
      );
  }

  onEditClicked(id, roleId): void {
    this.router.navigate([`./edit-service-user/${id}/${roleId}`], { relativeTo: this.route });
  }

  onFilter(filterValue: string) {
    this.filterBy.keyword = filterValue;
    this.paginator.pageIndex = 0;
    this.resolveUsers(this.filterBy);
  }

  onDeleteClicked(id) {

  }

  callServiceUser(serviceUserId, suFullname) {
    const body = {serviceUserId,suFullname};
    const token = localStorage.getItem('token');
    const kwName = JSON.parse(atob(token.split('.')[1])).fullName;
    this.meetService.createSession(body).subscribe(rsp => {
      const payload = {sender: this.loggedInUserId, senderName: kwName, receiver: serviceUserId,
        subscriptionType: 'CALL', callNotificationType: 'INCOMING_CALL'}
      this.serviceUserService.sendNotification(payload).subscribe();
      this.router.navigate(['/service-user/meet/join'], { relativeTo: this.route, 
        queryParams: {id: serviceUserId, fullName: suFullname}, queryParamsHandling: 'merge'});
    });
  }

  ngOnDestroy() {
    this.source.close();
  }
  dropdownFilter(obj) {
    this.filterBy['projectId'] = obj.id;
    this.paginator.pageIndex = 0;
    this.resolveUsers(this.filterBy);
  }
  fetchProjectList(filterBy) {
    this.serviceUserService.getProjectList(ApplicationConstant.clientId).subscribe(
      data => {
        data.forEach(element => {
          const projectObj = {
            'menuName': element.name,
            'id': element.id
          }
          this.filterArray.push(projectObj)
        });
      },
      error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
      }
    );
  }
}
