import { Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CompleteAssessmentsService } from './complete-assessments.service';
import { ServiceUserNavigation } from '../service-user-nav';
import { InPageNavService } from '../../shared/components/in-page-nav/in-page-nav.service';
import * as Survey from 'survey-angular';
import * as widgets from 'surveyjs-widgets';
import { AppConfirmService } from '../../../framework/components/app-confirm/app-confirm.service';
import { merge } from 'rxjs';

widgets.bootstrapslider(Survey);

@Component({
  selector: 'app-complete-assessments',
  templateUrl: './complete-assessments.component.html',
  styleUrls: ['./complete-assessments.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CompleteAssessmentsComponent implements OnInit, OnDestroy {
  fname: string;
  lname: string;
  prn: string;
  userId: string;
  displayedColumns: string[] = ['assessmentName', 'createdDate', 'modifiedDate', 'status', 'actions'];
  dataSource = new MatTableDataSource<any>();
  authenticatedUserId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userId;
  public pageSize = 10;
  allAssessments: any[] = [];
  status;
  todoTab;
  completedTab;
  allTab;

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort , {static:false}) sort: MatSort;
  constructor(
    private readonly _completeAssessmentsService: CompleteAssessmentsService,
    private readonly _route: ActivatedRoute,
    private readonly _router: Router,
    private readonly snack: MatSnackBar,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly appConfirmService: AppConfirmService,
  ) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
  }

  setTitle() {
    this._route.snapshot.parent.data['title']= '';
    this._route.queryParams.subscribe((params: any) => {
      if (params.fullName) {
        this.fname = params.fullName;
        this.userId = params.id;
      }
      this._route.snapshot.data['title'] = `${this.fname}`;
    });
  }

  ngOnInit() {
    this._route.url.subscribe(url =>{
      this.setTitle();
    })
    this._route.queryParams.subscribe((params: any) => {
      this.paginator.pageSize = this.pageSize;
      this.userId = params.id;
    });
    this._route.params.subscribe((param: any) =>{
      if(param.status){
        this.status = param.status;
      }
      this.getAllAssessments();
    })
  }
  onExitClicked() {
    this._router.navigate(['./service-user']);
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

  onDeleteClicked(id1,id2) {
    const dialogRef = this.appConfirmService.confirm({
      title: `Delete Assessment`,
      message: `Are you sure you want to delete Assessment?`,
      okButtonName: 'Yes',
      cancelButtonName: 'No'
    });

    dialogRef.subscribe(result => {
      if (result) {
        this._completeAssessmentsService.deleteAssessment(id1,id2).subscribe(
          data => {
            this.getAllAssessments();
            this.snack.open('Assessment deleted successfully!', 'Dismiss', { duration: 4000 });
          },
          (error: any) => {
            this.snack.open(error.errorMessage, 'Dismiss', { duration: 6000 });
          }
        );
      }
    });
  }

getAllAssessments() {
    this._completeAssessmentsService.getAssessmentListByLoggedInUser(this.userId).subscribe(
      response => {
        this.allAssessments = response;
        if(this.status){
          this.filterAssessments(this.status);
        }else{
          this.filterAssessments('todo');
        }
        
        this.paginator.length = response.length;
        this.dataSource.paginator = this.paginator;
      },
      error => this.snack.open(error.errorMessage, 'Dismiss', { duration: 4000 })
    );
  }
filterAssessments(status) {
  this.status = status;
  this.paginator.pageIndex = 0;

  switch (status) {
    case 'completed':
      this.dataSource.data = this.allAssessments.filter(obj => obj.isCompleted === true);
      this.completedTab = true;
      this.todoTab = false;
      this.allTab = false;
      break;
    case 'todo': 
      this.dataSource.data = this.allAssessments.filter(obj => obj.isCompleted === false);
      this.completedTab = false;
      this.todoTab = true;
      this.allTab = false;

      break;
    default:
      this.dataSource.data = this.allAssessments;
      this.completedTab = false;
      this.todoTab = false;
      this.allTab = true;

      break;
  }
  this._router.navigate([],{ queryParams: {status:null}, queryParamsHandling: "merge"});
}
}
