import { Component, EventEmitter, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IAssessmentTemplate } from '../../../assessment/assessment.interface';
import { CompleteAssessmentsService } from '../complete-assessments.service';
import { ServiceUserNavigation } from '../../service-user-nav';
import { InPageNavService } from '../../../shared/components/in-page-nav/in-page-nav.service';
import * as Survey from 'survey-angular';
import * as widgets from 'surveyjs-widgets';
import * as SurveyEditor from 'survey-creator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppConfirmService } from '../../../../framework/components/app-confirm/app-confirm.service';

widgets.bootstrapslider(Survey);

@Component({
  selector: 'app-complete-assessment',
  templateUrl: './complete-assessment.component.html',
  styleUrls: ['./complete-assessment.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CompleteAssessmentComponent implements OnInit, OnDestroy {
  fname: string;
  lname: string;
  prn: string;
  userId: string;
  json;
  answerJson;
  assessmentTemplate;
  assessmentName;
  surveyItem;
  length = 0;
  isCompleted: boolean = false;
  isPartiallyCompleted: boolean = false;
  assessmentTemplates: IAssessmentTemplate[];
  filteredAssessmentTemplate: IAssessmentTemplate[];
  private assessmentTemplateId: any;
  private assessmentTemplateUserId: any;
  noAssessmentMessage = 'Ups! The requested assessment is not available.';
  private score: number = 0;

  status;
  assessmentUrl;

  constructor(
    private readonly completeAssessmentsService: CompleteAssessmentsService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snack: MatSnackBar,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly _onConfirmService: AppConfirmService,
  ) {
    this.route.snapshot.parent.data['title'] ='';
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
  }


  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.fullName) {
        this.fname = params.fullName;
        this.userId = params.id;
        this.status = params.status;
      }
      this.route.snapshot.data['title'] = `${this.fname}`;
    });
  }

  ngOnInit() {
    this.assessmentUrl = 'service-user/assessments';
    this.getUserAssessmentJsons();
  }

  getUserAssessmentJsons() {
    this.route.queryParams.subscribe((params: any) => {
      this.assessmentTemplateId = params.assessmentTemplateId;
      this.assessmentTemplateUserId = params.assessmentTemplateUserId;
      this.isPartiallyCompleted = JSON.parse(params.isPartiallyCompleted);

      this.completeAssessmentsService.getAssessments(this.assessmentTemplateId).subscribe(
        data => {
          this.assessmentTemplate = data;
          this.assessmentName = this.assessmentTemplate.assessmentName;
          this.json = JSON.parse(this.assessmentTemplate.questionJson);
          this.surveyItem = new Survey.Model(this.json);
          if (this.isPartiallyCompleted) {
            this.completeAssessmentsService.getCompletedAssessment(this.assessmentTemplateUserId,this.assessmentTemplateId).subscribe(
              answerData => {
                this.answerJson = JSON.parse(answerData.answerJson);
                this.surveyItem.data = this.answerJson;
                this.length = Object.keys(this.answerJson).length;
              }
            )
          }
        },
        err => console.error(err),
        () => this.renderAssessment()
      );
    });
  }

  renderAssessment() {

    const surveyValueChanged = function (sender, options) {
      const question = sender.getQuestionByName(options.name);
      if (question && question.isRequired && options.value) {
        question.runValidators();
        question.clearErrors();
      } else if (question && question.isRequired) {
        question.hasErrors();
      }
    };

    const defaultThemeColorsEditor = Survey
      .StylesManager
      .ThemeColors['default'];
    defaultThemeColorsEditor['$primary-color'] = '#2b769b';
    defaultThemeColorsEditor['$secondary-color'] = '#2b769b';
    defaultThemeColorsEditor['$main-color'] = '#2b769b';
    Survey.StylesManager.applyTheme();

    Survey.SurveyNG.render('surveyElement', {
      model: this.surveyItem,
      onValueChanged: surveyValueChanged
    });
  }

  saveSurveyResponse(result) {
    const resultData = {
      userId: this.userId,
      userName: JSON.parse(atob(localStorage.getItem('token').split('.')[1])).fullName,
      assessmentTemplateId: this.assessmentTemplateId,
      assessmentTemplateUserId: this.assessmentTemplateUserId,
      answerJson: result.surveyItem.data,
      isCompleted: false,
      isPartiallyCompleted: true
    };

    if (this.isFormValueChanged(result)) {
      this.isCompleted = false;
      this.isPartiallyCompleted = true;
      this.saveAssessmentResult(resultData);
    } else {
      this.snack.open(`Select minimum of one answer!`, 'Dismiss', { duration: 10000 });
    }

  }

  completeSurveyResponse(result) {
    this.score = 0;
   
    const scoreQuestion = result.surveyItem.getQuestionByName("score");
    
    if(scoreQuestion){
      const keys = Object.keys(result.surveyItem.data);
      keys.forEach(questionName => {
        const element = result.surveyItem.getQuestionByName(questionName);
        if (element && element.getType() === 'checkbox'){
          const itemValue = result.surveyItem.data[questionName];
          itemValue.forEach(checkBoxOptions => {
            const count = this.getScore(questionName, checkBoxOptions);
            this.score = this.score + count;
          });
        }else if (element && element.getType() === 'bootstrapslider'){
          const itemValue = result.surveyItem.data[questionName];
          this.score = this.score +  Number(itemValue);
        }else if(element && element.getType() !== 'expression' && element.getType() !== 'html'
               && element.getType() !=='comment' ){
          const itemValue = result.surveyItem.data[questionName];
          const count = this.getScore(questionName, itemValue);
          this.score = this.score +  count;
        }
      });
      scoreQuestion.value = this.score;
    }

    if (!result.surveyItem.checkIsCurrentPageHasErrors()) {
      const resultData = {
        userId: this.userId,
        userName: JSON.parse(atob(localStorage.getItem('token').split('.')[1])).fullName,
        assessmentTemplateId: this.assessmentTemplateId,
        assessmentTemplateUserId: this.assessmentTemplateUserId,
        answerJson: result.surveyItem.data,
        isCompleted: true,
        isPartiallyCompleted: false
      };
      this.isCompleted = true;
      this.isPartiallyCompleted = false;
      this.saveAssessmentResult(resultData)
    }

  }

  getScore(questionName, itemValue){
    let count = 0;
    const keys = Object.keys(this.json);
    keys.forEach(element => {
      if('pages' === element){
        const pages = this.json.pages;
        pages.forEach(page => {
          count = this.getScoreByQuestion(page, questionName, itemValue);
        });
      
      }
    });
    return count;
  }

  getScoreByQuestion(page, questionName, itemValue){
    let count = 0
    const  elements = page.elements;
    elements.forEach(question => {
      if(question.name === questionName){
        count = this.getCount(question, itemValue);
      }
    });

    return count;
  }

  getCount(question, itemValue){
    let count = 0;
    if(question.rateValues && question.type === 'rating'){
      question.rateValues.forEach(choice => {
        if(choice.value === itemValue && choice.score){
          count = Number(choice.score);
        }
      });
    }else if( question.choices ){
      question.choices.forEach(choice => {
        if(choice.score && choice.value === itemValue){
          count = Number(choice.score);
        }
      });
    }
    return count;
  }

  saveAssessmentResult(resultData) {
    this.completeAssessmentsService.saveAssessmentResult(resultData).subscribe(
      (data: any) => {
        if (this.isPartiallyCompleted) {
          this.snack.open(`Assessment saved successfully!`, 'Dismiss', { duration: 10000 });
          this.router.navigate([this.assessmentUrl, this.status],
            { queryParams: { id: this.userId, fullName: this.fname, status: this.status } });
        } else if (this.isCompleted) {
          this.snack.open(`Assessment completed successfully!`, 'Dismiss', { duration: 10000 });
          this.router.navigate([this.assessmentUrl, this.status],
            { queryParams: { id: this.userId, fullName: this.fname, status: 'completed' } });
        }
      },
      (error: any) => {
        this.snack.open(error.errorMessage, 'Dismiss', { duration: 6000 });
        this.router.navigate([this.assessmentUrl, this.status],
          { queryParams: { id: this.userId, fullName: this.fname, } });
      }
    );
  }
  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

  onExitClicked(result) {
    if (this.isExitAllowed(result)) {
      const dialogRef = this._onConfirmService.confirm({
        title: `Exit Assessment`,
        message: `You have not saved your progress, do you want to continue without saving?`,
        okButtonName: 'Yes',
        cancelButtonName: 'No'
      });

      dialogRef.subscribe(output => {
        if (output) {
          this.router.navigate(['./service-user/assessments', this.status],
          { queryParams: { id: this.userId, fullName: this.fname, status: this.status } });
        }
      });
    } else {
      this.router.navigate(['./service-user/assessments', this.status],
      { queryParams: { id: this.userId, fullName: this.fname, status: this.status } });
    }

  }

  isFormValueChanged(result) {
    let isFormValueChanged = false;
    if (result.surveyItem.data) {
      const keys = Object.keys(result.surveyItem.data);
      if (keys && keys.length > 0) {
        keys.forEach(element => {
          if (result.surveyItem.getQuestionByName(element) &&
           result.surveyItem.getQuestionByName(element).getType() !== 'expression') {
            isFormValueChanged = true;
          }
        });
      }
    }

    return isFormValueChanged;
  }

  isExitAllowed(result) {
    let isFormValueChanged = false;
    const keys = Object.keys(result.surveyItem.data);
    let answerKeys = null;
    if (this.answerJson !== null && this.answerJson !== undefined) {
      answerKeys = Object.keys(this.answerJson);
    }

    if (keys && answerKeys && keys.length !== answerKeys.length) {
      isFormValueChanged = true;
    }

    if (keys && keys.length > 0 && !isFormValueChanged) {
      keys.forEach(element => {
        if (keys && keys.length > 0 && (answerKeys === null || answerKeys === undefined) && result.surveyItem.getQuestionByName(element).getType() !== 'expression') {
          isFormValueChanged = true;
        } else if (result.surveyItem.getQuestionByName(element).getType() !== 'expression' && !isFormValueChanged && this.answerJson[element] !== result.surveyItem.data[element]) {
          isFormValueChanged = true;
        }
      });
    }
    return isFormValueChanged;
  }
}
