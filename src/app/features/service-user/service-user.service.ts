import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';
import { BaseUrl } from '../../framework/constants/url-constants';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceUserService {

  constructor(private readonly http: HttpClient) { }

  getServiceUsers(sort: string, size: number, page: number, body: any): Observable<any> {
    const href = `${BaseUrl.USER}/user/search`;
    return this.http.post<any>(href, body, {
      params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString())
        .set('sort', sort.toString())
    });
  }

  saveNewServiceUser(payload) {
    const href = `${BaseUrl.USER}/serviceUser/create`;
    return this.http.post<any>(href, payload);
  }

  getProfileDetails(id): Observable<any> {
    const href = `${BaseUrl.USER}/serviceUser/get/${id}`;
    return this.http.get<any>(href);
  }
  getServiceUserDetails(id: number) {
    const href = `${BaseUrl.USER}/serviceUser/get/${id}`;
    return this.http.get<any>(href);
  }

  editServiceUser(payload) {
    const href = `${BaseUrl.USER}/serviceUser/update`;
    return this.http.put<any>(href, payload);
  }

  resetPassword(id) {
    const href = `${BaseUrl.USER}/su/reset-password/${id}`;
    return this.http.post<any>(href, {"allowEmailRegistration": true});
  }

  getListOfGenders(): Observable<any> {
    const href = `${BaseUrl.USER}/serviceUser/genders`;
    return this.http.get<any>(href);
  }
  getCVDetails(uid) {
    const href = `${BaseUrl.CVB}/cvs/${uid}`;
    return this.http.get<any>(href);
  }

  download(id): any {
    const href = `${BaseUrl.CVB}/download/cvs/${id}`;
    this.createAndSubmitForm(href);
  }
  downloadNationalJobCv(id): any {
    const href = `${BaseUrl.CVB}/download/cvs/national/${id}`;
    this.createAndSubmitForm(href);
  }
  createAndSubmitForm(url: string): void {
    const fd = document.createElement('form');
    fd.setAttribute('action', url);
    fd.setAttribute('method', 'POST');
    const inputElem = document.createElement('input');
    inputElem.setAttribute('name', 'access_token');
    inputElem.setAttribute('value', 'Bearer ' + localStorage.getItem('token'));
    fd.appendChild(inputElem);
    const holder = document.getElementById('form_holder');
    holder.appendChild(fd);
    fd.submit();
    holder.removeChild(fd);
  }

  getFavJobList(suid, page, size, sort) {
    const href = `${BaseUrl.JOBS}/job-user/favourites/${suid}`;
    return this.http.get<any>(href, {
      params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString())
        .set('sort', sort.toString())
    });

  }

  getJobDetails(id: string) {
    const href = `${BaseUrl.JOBS}/jobs/${id}`;
    return this.http.get<any>(href);
  }

  getJobApplications(suid, page, size, sort) {
    const href = `${BaseUrl.JOBS}/applications/service-users/${suid}`;
    return this.http.get<any>(href, {
      params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString())
        .set('sort', sort.toString())
    });
  }

  getLocalJobs(suid, page, size, sort) {
    const href = `${BaseUrl.JOBS}/express-interest/nfn/${suid}`;
    return this.http.get<any>(href, {
      params: new HttpParams()
        .set('page', page.toString())
        .set('size', size.toString())
        .set('sort', sort.toString())
    });
  }

  sendNotification(payload) {
    const href = `${BaseUrl.USER}/call/notification`;
    return this.http.post<any>(href, payload);
  }

  broadcast(payload) {
    const href = `${BaseUrl.USER}/call/broadcast`;
    return this.http.post<any>(href, payload);
  }

  unsubscribe(id, event) {
    const href = `${BaseUrl.USER}/call/unsubscribe/${id}/${event}`;
    return this.http.get(href);
  }
  getGoals(userId) {
    const href = `${BaseUrl.PLAN}/induction-Plan/goals/1?userId=${userId}`;
    return this.http.get<any>(href);
  }

  getHistoryData(userId) {
    const href = `${BaseUrl.PLAN}/induction-history/timeLineHistory?userId=${userId}`;
    return this.http.get<any>(href);
  }

  getSchedulerData(userId) {
    const href = `${BaseUrl.PLAN}/schedule?userId=${userId}`;
    return this.http.get<any>(href);
  }

  getFstructureData(userId, pageNumber) {
    const href = `${BaseUrl.PLAN}/fstructure?sort=sortDate,desc&sort=activityId,asc&page=${pageNumber}&size=15&userId=${userId}`;
    return this.http.get<any>(href);
  }

  getEthnicityRefData(): Observable<any> {
    const href = `${BaseUrl.USER}/ref-data/ethnicity`;
    return this.http.get<any>(href);
  }

  getRefAnswer(): Observable<any> {
    const href = `${BaseUrl.USER}/ref-data/refAnswer`;
    return this.http.get<any>(href);
  }

  getRefSentencData(): Observable<any> {
    const href = `${BaseUrl.USER}/ref-data/sentence`;
    return this.http.get<any>(href);
  }
  getRefLLIFProjectData(): Observable<any> {
    const href = `${BaseUrl.USER}/ref-data/llif-project`;
    return this.http.get<any>(href);
  }
  public requestDataFromMultipleSources(id) {
    const ethnicityResp = this.http.get(`${BaseUrl.USER}/ref-data/ethnicity`);
    const profileDetailsResp = this.http.get(`${BaseUrl.USER}/serviceUser/get/${id}`);
    const listOfGenderResp = this.http.get(`${BaseUrl.USER}/serviceUser/genders`);
    const refSentanceData = this.http.get(`${BaseUrl.USER}/ref-data/sentence`);
    const llifProjectData = this.http.get(`${BaseUrl.USER}/ref-data/llif-project`);
    return forkJoin({ ethnicityResp: ethnicityResp, profileDetailsResp: profileDetailsResp,
       listOfGenderResp: listOfGenderResp, refSentanceData: refSentanceData,
       llifProjectData: llifProjectData });
  }
  getProjectList(clientId): Observable<any> {
    const href = `${BaseUrl.USER}/ref-data/projects/${clientId}`;
    return this.http.get<any>(href);
  }
}
