import { Injectable } from '@angular/core';

const serviceUserRoute = 'service-user';

@Injectable()
export class ServiceUserNavigation {
  serviceUserSubMenu = {

    sectionName: 'User Profile',
    featureId: [11],
    description: ``,
    state: null,
    icon: 'bar_chart',
    menuItems: [
      {
        name: 'Profile',
        featureId: [9],
        description: ``,
        state: `${serviceUserRoute}/profile-tabs`,
        icon: null,
        submenu: []
      },
      {
        name: 'Plan',
        featureId: [30],
        description: ``,
        state: `${serviceUserRoute}/plan`,
        icon: null,
        submenu: []
      },
      {
        name: 'Assessments',
        featureId: [31],
        description: ``,
        state: `${serviceUserRoute}/assessments`,
        icon: null,
        submenu: []
      },
      {
        name: "CVs",
        featureId: [32],
        description: ``,
        state: `${serviceUserRoute}/cv`,
        icon: null,
        submenu: []
      },
      {
        name: 'Documents',
        featureId: [33],
        description: ``,
        state: `${serviceUserRoute}/documents`,
        icon: null,
        submenu: []
      },
      {
        name: 'Job Opportunities',
        featureId: [29],
        description: ``,
        state: `${serviceUserRoute}/job-activity`,
        icon: null,
        submenu: []
      }
    ]
  };
}
