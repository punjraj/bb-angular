import { Component, OnInit } from '@angular/core';
import { ServiceUserService } from '../service-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InPageNavService } from '../../shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../service-user-nav';

@Component({
  selector: 'app-job-activity',
  templateUrl: './job-activity.component.html',
  styleUrls: ['./job-activity.component.scss']
})
export class JobActivityComponent implements OnInit {
  userId: string;
  fname: string;
  lname: string;
  SU: string = 'SU';
  favouritesJobs: boolean = false;
  jobApplications: boolean = false;
  prisonJobs: boolean = false;
  localJobs: boolean = false;

  constructor(private readonly serviceUserService: ServiceUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
  ) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
     this.jobActivityTabs('localJobs');
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.id) {
        this.userId = params.id;
        this.fname = params.fullName;
        this.userId = params.id;
      }
      this.route.snapshot.data['title'] = `${this.fname}`;
    });
  }
  jobActivityTabs(tabName: string) {
    switch (tabName) {
      case 'favouritesJobs':
        
        this.favouritesJobs = true;
        this.jobApplications = false;
        this.prisonJobs = false;
        this.localJobs = false;
        break;
      case 'jobApllications':
        this.favouritesJobs = false;
        this.jobApplications = true;
        this.prisonJobs = false;
        this.localJobs = false;
        break;
      case 'prisonJobs':
        this.favouritesJobs = false;
        this.jobApplications = false;
        this.prisonJobs = true;
        this.localJobs = false;
        this.router.navigate(['/service-user/job-activity/prison-jobs'],
      { queryParams: { id: this.userId, firstName: this.fname, lastName: this.lname, SU: this.SU }});

        break;
      case 'localJobs':
        this.favouritesJobs = false;
        this.jobApplications = false;
        this.prisonJobs = false;
        this.localJobs = true;
        break;
    }
  }
  ngOnInit() {
  }

}
