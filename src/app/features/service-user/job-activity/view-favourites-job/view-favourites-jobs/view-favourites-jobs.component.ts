import { Component, OnInit } from '@angular/core';
import { ServiceUserService } from '../../../service-user.service';
import { Router, ActivatedRoute } from '@angular/router';

import { JobDetails } from './JobDetails.interface';
import { Location } from '@angular/common';
import { ServiceUserNavigation } from '../../../service-user-nav';
import { SnackBarService } from '../../../../../framework/service/snack-bar.service';
import { InPageNavService } from '../../../../../features/shared/components/in-page-nav/in-page-nav.service';


@Component({
  selector: 'app-view-favourites-jobs',
  templateUrl: './view-favourites-jobs.component.html',
  styleUrls: ['./view-favourites-jobs.component.scss']
})
export class ViewFavouritesJobsComponent implements OnInit {
  job:JobDetails;
  userId: string;
  fname: string;
  lname: string;
  prn: string;
  constructor( private readonly serviceUserService: ServiceUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly location: Location,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,) { 
      this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
      this.setTitle();}
    
    setTitle() {
      this.route.queryParams.subscribe((params: any) => {
        if (params.id) {
          this.fname = params.fullName;
          }
        this.route.snapshot.data['title'] = `${this.fname}`;
      });
    }
  
    ngOnInit() {
      this.route.queryParams.subscribe((params: any) => {
        if (!params.jobid) {
          this.router.navigate(['../'], { relativeTo: this.route });
        } else {
          this.serviceUserService.getJobDetails(params.jobid)
          .subscribe(jobDetails => {
            this.job = jobDetails;
            console.log('this.job: ', this.job)
            console.log( this.job.job_title);
          },
          error => {
            this.snackBarService.error(`${error.error.applicationMessage}`);
            this.router.navigate(['../../']);
          });
        }
      });
    }

    ngOnDestroy() {
      this.inPageNavService.setNavItems(null);
    }

    goBack() {
      this.location.back();
    }
  
  }
