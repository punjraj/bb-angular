import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceUserNavigation } from '../../service-user-nav';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ServiceUserService } from '../../service-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/internal/operators/tap';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { InPageNavService } from '../../../../features/shared/components/in-page-nav/in-page-nav.service';

@Component({
  selector: 'app-local-jobs',
  templateUrl: './local-jobs.component.html',
  styleUrls: ['./local-jobs.component.scss']
})
export class LocalJobsComponent implements OnInit {
  page: number = 0;
  pageSize: number = 10;
  sortDireAndColumn: any = 'dateExpressed,desc';
  totalItems: number = 0;
  jobId: string;
  userId: string;
  fullName: string;
  SU: string;
  authenticatedUserOrgId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).orgId;
  displayedColumns: string[] = ['dateExpressed', 'jobAdvertNFN.jobTitle', 'jobAdvertNFN.location.description', 'jobStatus', 'status'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private readonly serviceUserService: ServiceUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      this.fullName = params.fullName;
      this.SU = params.SU;
      this.userId = params.id;
    });
  }
  ngAfterViewInit() {
    this.sort.sortChange.subscribe((data) => {
      this.sortDireAndColumn = `${data.active},${data.direction}`;
      this.paginator.pageIndex = 0;
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.resolveUserLocalJobs();
          document.querySelector('#local-jobs').scrollIntoView();
        })
      )
      .subscribe();
    this.resolveUserLocalJobs();
  }
  resolveUserLocalJobs() {
    this.serviceUserService.getLocalJobs(this.userId, this.paginator.pageIndex, this.pageSize, this.sortDireAndColumn)
      .subscribe(data => {
        this.dataSource.data = data.content;
        this.paginator.length = data.totalElements;
      }, error => {
        this.snackBarService.error(`${error.error.errorMessage}`);
        this.router.navigate(['./local-jobs']);
      });
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

  onExitClicked() {
    this.router.navigate(['./service-user']);
  }
  viewLocalJob(jobId: any) {
    this.router.navigate(['/service-user/job-activity/local-jobs/su-view-local-jobs/', jobId],
      { relativeTo: this.route, queryParams: { fullName: this.fullName, SU: this.SU, id: this.userId } });
  }
  onPaginateChange(event) {
    document.querySelector('#applications').scrollIntoView();
  }
}
