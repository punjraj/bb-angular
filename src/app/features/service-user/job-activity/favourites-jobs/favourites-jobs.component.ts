import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ServiceUserService } from '../../service-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { tap } from 'rxjs/internal/operators/tap';
import { InPageNavService } from '../../../shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../../service-user-nav';
import { Location } from '@angular/common';


@Component({
  selector: 'app-favourites-jobs',
  templateUrl: './favourites-jobs.component.html',
  styleUrls: ['./favourites-jobs.component.scss']
})
export class FavouritesJobsComponent implements OnInit {
  displayedColumns: string[] = ['job_title', 'date', 'location_name', 'expired'];
  dataSource = new MatTableDataSource<any>();

  page: number = 0;
  pageSize: number = 10;
  sortDireAndColumn: any = 'date,desc';
  favJobList: any[];
  totalItems: number = 0;
  jobId: string;
  userId: string;
  fname: string;
  lname: string;
  prn: string;

  @ViewChild(MatPaginator,{static:false}) paginator: MatPaginator;
  
  constructor( private readonly serviceUserService: ServiceUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly location: Location,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,


   ) { this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();}
  
    setTitle() {
      this.route.queryParams.subscribe((params: any) => {
        if (params.id) {
          this.fname = params.fullName;
          this.userId = params.id;
          
        }
        this.route.snapshot.data['title'] = `${this.fname}`;
      });
    }

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => { 
      this.userId = params.id;
      this.paginator.pageIndex=0;
      this.resolveUsers();
    });
  }

  ngAfterViewInit() {
   this.paginator.page
      .pipe(
        tap(() => {
          this.resolveUsers();
          document.querySelector('#favorites-jobs').scrollIntoView();
        })
      )
      .subscribe();
  }
    resolveUsers() {
      this.serviceUserService.getFavJobList( this.userId, this.paginator.pageIndex, this.pageSize, this.sortDireAndColumn)
      .subscribe(data => {
        this.dataSource.data =data.content;
          this.paginator.length = data.totalElements;
        },error => {
          this.snackBarService.error(`${error.error.applicationMessage}`);
          this.router.navigate(['./favourites']);
        });
    }

    ngOnDestroy() {
      this.inPageNavService.setNavItems(null);
    }

    onExitClicked() {
      this.router.navigate(['./service-user']);
    }

}
