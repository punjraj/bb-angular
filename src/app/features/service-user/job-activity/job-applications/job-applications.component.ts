import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceUserNavigation } from '../../service-user-nav';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ServiceUserService } from '../../service-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { tap } from 'rxjs/internal/operators/tap';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
import { InPageNavService } from '../../../shared/components/in-page-nav/in-page-nav.service';
@Component({
  selector: 'app-job-applications',
  templateUrl: './job-applications.component.html',
  styleUrls: ['./job-applications.component.scss']
})
export class JobApplicationsComponent implements OnInit {
  displayedColumns: string[] = ['applicationStatusDate', 'jobStatus', 'jobTitle', 'jobApplicationStatus.statusDescription'];
  dataSource = new MatTableDataSource<any>();
  authenticatedUserId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userId;

  page: number = 0;
  pageSize: number = 10;
  sortDireAndColumn: any = 'applicationStatusDate,desc';
  totalItems: number = 0;
  jobId: string;
  userId: string;
  fname: string;
  lname: string;
  prn: string;
  serviceUser: any;
  flag: boolean = true;
  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;

  constructor(private readonly serviceUserService: ServiceUserService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly snackBarService: SnackBarService,
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,

  ) { this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu); }

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      this.fname = params.firstName;
      this.userId = params.id;
      this.paginator.pageIndex = 0;
      this.resolveUsersJobApplications();
    });
  }
  ngAfterViewInit() {
    this.sort.sortChange.subscribe((data)=>{
      this.sortDireAndColumn = `${data.active},${data.direction}`;
      this.paginator.pageIndex = 0;
      this.resolveUsersJobApplications();
    });

    this.paginator.page
      .pipe(
        tap(() => {
          this.resolveUsersJobApplications();
          document.querySelector('#applications').scrollIntoView();
        })
      )
      .subscribe();
  }
  resolveUsersJobApplications() {
    this.serviceUserService.getJobApplications(this.userId, this.paginator.pageIndex, this.pageSize, this.sortDireAndColumn)
      .subscribe(data => {
        this.dataSource.data = data.content;
        this.paginator.length = data.totalElements;
      }, error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
        this.router.navigate(['./job-applications']);
      });
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

  onExitClicked() {
    this.router.navigate(['./service-user']);
  }

  viewApplication(userId: any, applicationId: any) {
    this.serviceUserService.getServiceUserDetails(userId)
      .subscribe(info => {
        this.serviceUser = info.firstName + ' ' + info.surName + '-' + info.prn;
        this.router.navigate(['/service-user/job-activity/job-applications/view-application/', applicationId],
          { relativeTo: this.route, queryParams: { firstName:info.firstName, lastName:info.surName, prn:info.prn, id: userId } });
      });

  }
  onPaginateChange(event) {
    document.querySelector('#applications').scrollIntoView();
  }
}
