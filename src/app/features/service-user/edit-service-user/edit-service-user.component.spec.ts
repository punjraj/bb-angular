import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EditServiceUserComponent } from './edit-service-user.component';

describe('EditServiceUserComponent', () => {
  let component: EditServiceUserComponent;
  let fixture: ComponentFixture<EditServiceUserComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EditServiceUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditServiceUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
