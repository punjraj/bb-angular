import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../../framework/components/date-adapter/date-adapter';
import { ServiceUserService } from '../service-user.service';
import { SnackBarService } from '../../../framework/service/snack-bar.service';
import { Utility } from '../../../framework/utils/utility';
import { PdfBookmarkComponent } from 'ngx-extended-pdf-viewer';

@Component({
  selector: 'app-edit-service-user',
  templateUrl: './edit-service-user.component.html',
  styleUrls: ['./edit-service-user.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ]
})
export class EditServiceUserComponent implements OnInit {

  serviceUserForm: FormGroup;

  maxDateOfBirth = new Date();
  minDateOfBirth = new Date(new Date().setFullYear(new Date().getFullYear() - 100));
  currentDate = new Date(new Date().setHours(0, 0, 0, 0));
  lrnPattern = `[a-zA-Z0-9\ ]+`;
  prnPattern = `[A-Z][0-9]{4}[A-Z]{2}`;
  namePattern = `[^0-9\\r\\n\\t|\"]+$`;
  emailPattern = /^[a-z0-9._%+'-]+@[a-z0-9.-]+\.[a-z]{2,}$/i;
  postCodePattern = /^(([A-Z][0-9]{1,2})|(([A-Z][A-HJ-Y][0-9]{1,2})|(([A-Z][0-9][A-Z])|([A-Z][A-HJ-Y][0-9]?[A-Z])))) [0-9][A-Z]{2}$/i;
  firstNamePattern = `([a-zA-Z]{1,})`;
  lastNamePattern = `([a-zA-Z]{1,}(\\s?|'?|-?)[a-zA-Z]{1,})`;
  onlyCharsPattern = /^[a-zA-Z]+[\-'\s]?[a-zA-Z ]+$/;
  phoneNumberPattern = /^(\d{7}|\d{11})$/;
  number = /^-?(0|[1-9]\d*)?$/;
  ethnicityRefData;

  titles = ['Ms', 'Mx', 'Mrs', 'Miss', 'Mr', 'Dr', 'Prof', 'Lady', 'Sir', 'Rev', 'Lord', 'Maj',
    'Capt', 'Pte', 'Sgt', 'Ssgt', 'Lt', 'Col', 'Brig', 'Cpl', 'Lcpl', '2Lt', 'WO1', 'WO2', 'Master', 'Other', 'N/A'];

  isNew = false;
  genderList: any;
  deactivated: boolean;
  ethnicityList;
  refAnswer;
  refEstablishment;
  refLocalAuth;
  refStatus;
  refProject;
  refRecallReason;
  refWithdrawnReason;

  constructor(
    private readonly fb: FormBuilder,
    private readonly snackBarService: SnackBarService,
    private readonly serviceUserService: ServiceUserService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) {
    this.serviceUserService.getEthnicityRefData().subscribe(data => {
      this.ethnicityRefData = data;
    });
    this.serviceUserService.getRefAnswer().subscribe(data => {
      this.refAnswer = data;
    });
    this.serviceUserService.getRefSentencData().subscribe(data => {
      this.refEstablishment = data.refEstablishment;
      this.refLocalAuth = data.refLocalAuthority;
    });

    this.serviceUserService.getRefLLIFProjectData().subscribe(data => {
      this.refStatus = data.refStatus;
      this.refRecallReason = data.refRecallReason;
      this.refWithdrawnReason = data.refWithdrawalReason;
      this.refProject = data.refProject;
    });

  }

  ngOnInit() {
    this.initServiceUserForm();
    this.serviceUserService.getListOfGenders().subscribe(data => {
      this.genderList = data;
    });
  }

  navigateHome() {
    this.router.navigate(['./service-user']);
  }

  initServiceUserForm() {
    this.createServiceUserForm();
    this.route.params.subscribe((params: any) => {
      if (params.hasOwnProperty('id')) {
        this.isNew = false;
        this.serviceUserService.getServiceUserDetails(+params['id'])
          .subscribe(
            (data: any) => {
              this.deactivated = !data.isActive;
              if (data.address) {
                data.address.email = data.address.emailAddress
              }
              this.serviceUserForm.patchValue(data);
              this.serviceUserForm.markAsTouched();
              if (data.dateOfBirth) {
                this.serviceUserForm.get('dateOfBirth').setValue(new Date(data.dateOfBirth));
              }
              this.serviceUserForm.get('address').get('email').disable();
              if (data.prn) {
                this.serviceUserForm.get('dateOfBirth').disable();
              }
            },

            error => {
              this.snackBarService.error(`${error.error.applicationMessage}`);
              this.navigateHome();
            }
          );
      } else {
        this.isNew = true;
      }
    });
  }

  createServiceUserForm() {
    this.serviceUserForm = this.fb.group({
      id: '',
      title: [null],
      firstName: [null, [Validators.required, Validators.maxLength(35), Validators.pattern(this.firstNamePattern)]],
      middleName: [null, [Validators.maxLength(35), Validators.pattern(this.namePattern)]],
      lastName: [null, [Validators.required, Validators.maxLength(35), Validators.pattern(this.lastNamePattern)]],
      email: [null],
      dateOfBirth: [null],
      gender: [null],
      postCode: [null],
      suRegType: ['NON_JUSTICE'],
      aliasFirstName: [null, [Validators.maxLength(35), Validators.pattern(this.firstNamePattern)]],
      aliasLastName: [null, [Validators.maxLength(35), Validators.pattern(this.lastNamePattern)]],
      ethnicityId: [null],
      isActive: [true],
      projectId: [null],
      deactivationDate: [null],
      address: this.getAddressForm(),
      disabilityAndHealth: this.getDisabilityAndHealthForm(),
      identifiers: this.getIdentifiersForm(),
      sentence: this.getSentenceDetailsForm(),
      llifProject: this.getLLIFProjectForm(),
      emotionalSupport: this.getEmotionalSupportForm(),
      employment: this.getEmploymentForm()
    });
  }

  onSaveNewUser(userForm) {
    const payload = userForm.getRawValue();
    payload.dateOfBirth = Utility.transformDateToString(payload.dateOfBirth);
    payload.email = payload.address.email;
    payload.address.emailAddress = payload.address.email
    payload.postCode = payload.address.postCode;
    payload.llifProject.recallDate = Utility.transformDateToString(payload.llifProject.recallDate);
    payload.llifProject.dateParticipantConsentPilot = Utility.transformDateToString(payload.llifProject.dateParticipantConsentPilot);
    payload.llifProject.withdrawnDate = Utility.transformDateToString(payload.llifProject.withdrawnDate);
    payload.llifProject.dateCompletedPilot = Utility.transformDateToString(payload.llifProject.dateCompletedPilot);
    this.serviceUserService.saveNewServiceUser(payload).subscribe(response => {
      this.navigateHome();
      this.snackBarService.success(`User successfully registered. Please ask the user to check their email inbox`);
    }, error => {
      this.errorCommonMethod(error);
    });
  }

  onUpdateUser(userForm) {
    const payload = userForm.getRawValue();
    payload.dateOfBirth = Utility.transformDateToString(payload.dateOfBirth);
    payload.email = payload.address.email;
    payload.address.emailAddress = payload.address.email
    payload.postCode = payload.address.postCode;
    payload.llifProject.recallDate = Utility.transformDateToString(payload.llifProject.recallDate);
    payload.llifProject.dateParticipantConsentPilot = Utility.transformDateToString(payload.llifProject.dateParticipantConsentPilot);
    payload.llifProject.withdrawnDate = Utility.transformDateToString(payload.llifProject.withdrawnDate);
    payload.llifProject.dateCompletedPilot = Utility.transformDateToString(payload.llifProject.dateCompletedPilot);

    this.serviceUserService.editServiceUser(payload).subscribe(response => {
      this.navigateHome();
      this.snackBarService.success(`Success! The ${payload.email} user is saved`);
    }, error => {
      this.errorCommonMethod(error);
    });
  }

  errorCommonMethod(error) {
    if (error.error.errors) {
      this.snackBarService.error(`${error.error.errors[0].errorMessage}`);
    } else {
      this.snackBarService.error(`${error.error.applicationMessage}`);
    }
  }

  onSubmit(userForm) {
    if (this.isNew) {
      this.onSaveNewUser(userForm);
    } else {
      this.onUpdateUser(userForm);
    }
  }

  getAddressForm() {
    const addressForm = this.fb.group({
      id: [null],
      addressLine1: [null, [Validators.required, Validators.maxLength(100)]],
      addressLine2: [null, [Validators.maxLength(100)]],
      townOrCity: [null, [Validators.maxLength(50), Validators.pattern(this.onlyCharsPattern)]],
      country: [null, [Validators.maxLength(50), Validators.pattern(this.onlyCharsPattern)]],
      postCode: [null, [Validators.required, Validators.pattern(this.postCodePattern)]],
      email: [null, [Validators.required, Validators.maxLength(100), Validators.pattern(this.emailPattern)]],
      contactNo: [null, Validators.pattern(this.phoneNumberPattern)]
    })
    return addressForm;
  }

  getDisabilityAndHealthForm() {
    const disabilityAndHealthForm = this.fb.group({
      id: [null],
      learningDifficultyId: [null],
      learningDifficultyDescription: [null, [Validators.maxLength(400)]],
      longTermHealthCondtionId: [null],
      longTermHealthCondtionDescription: [null, [Validators.maxLength(400)]],
      disabilityOrHealthCondtionId: [null],
      disabilityOrHealthCondtionDescription: [null],
    });
    return disabilityAndHealthForm;
  }

  getEmotionalSupportForm() {
    const emotionalSupportForm = this.fb.group({
      id: [null],
      completedProgramme: [null],
      qualifyForPeerMentoringSessions: [null]
    })
    return emotionalSupportForm;
  }

  getEmploymentForm() {
    const employmentForm = this.fb.group({
      id: [null],
      completedEmpTrainingInCustody: [null],
      completedEmpTrainingInCommunity: [null],
      achievedQualification: [null],
      qualificationOrSkillGained: [null],
      workPlacement: [null],
      gainedEmployment: [null],
      employerAndRole: [null, [Validators.maxLength(100)]],
      startDate: [null],
      postEmploymentSupport: [null],
      inEmploymentMoreThanSixMonths: [null],
    })
    return employmentForm;
  }

  getFormByName(formName) {
    return this.serviceUserForm.get(formName) as FormGroup
  }

  showField(control, dependendCtrl) {
    const form = this.serviceUserForm.get('disabilityAndHealth') as FormGroup
    if (form.get(control).value === 1) {
      form.get(dependendCtrl).setValidators([Validators.maxLength(400)]);
      form.get(dependendCtrl).updateValueAndValidity();
      return true
    }
    form.get(dependendCtrl).reset();
    form.get(dependendCtrl).clearValidators();
    form.get(dependendCtrl).updateValueAndValidity();
    return false
  }

  showdependentField(control, dependendCtrl) {
    const form = this.serviceUserForm.get('llifProject') as FormGroup
    if (form.get(control).value === 2) {
      form.get(dependendCtrl).updateValueAndValidity();
      return true
    }
    form.get(dependendCtrl).reset();
    form.get(dependendCtrl).clearValidators();
    form.get(dependendCtrl).updateValueAndValidity();
    return false
  }

  getIdentifiersForm() {
    const identifiersForm = this.fb.group({
      id: [null,],
      pnomisId: [null, [Validators.maxLength(35), Validators.pattern(this.lrnPattern)]],
      pncNo: [null, [Validators.maxLength(35), Validators.pattern(this.lrnPattern)]],
      deliusNo: [null, [Validators.maxLength(35), Validators.pattern(this.lrnPattern)]],
    })
    return identifiersForm
  }

  getSentenceDetailsForm() {
    const sentenceDetailsForm = this.fb.group({
      id: [null],
      bookingCode: [null, [Validators.maxLength(35), Validators.pattern(this.lrnPattern)]],
      noOfOffence: [null, Validators.pattern(this.number)],
      mainIndexOffence: [null, Validators.maxLength(300)],
      sentenceDate: [null,],
      sentenceLengthMonth: [null, Validators.pattern(this.number)],
      sentenceLengthDays: [null, Validators.pattern(this.number)],
      actualReleaseDate: [null,],
      establishmentId: [null,],
      probationOfficeReleased: [null, Validators.maxLength(100)],
      localAuthorityId: [null,],
    })
    return sentenceDetailsForm
  }

  getLLIFProjectForm() {
    const LLIFfProjectForm = this.fb.group({
      id: [null,],
      participantNumber: [null, [Validators.maxLength(35), Validators.pattern(this.lrnPattern)]],
      dateParticipantConsentPilot: [null,],
      recalled: [1, Validators.required],
      recallDate: [null,],
      recallReasonId: [null,],
      withdrawnPilot: [1, Validators.required],
      withdrawnDate: [null,],
      withdrawalReasonId: [null,],
      iirpDeveloped: [1, Validators.required],
      smsCoordinated: [1, Validators.required],
      smsAccessed: [1, Validators.required],
      familyReintegrationInterventionProvided: [1,],
      supportNetworkIdentified: [null,],
      plReportsImprovedRelationships: [null,],
      plHasFamilySupportUponRelease: [null,],
      plFamilyReportsImprovedRelationships: [null,],
      dateCompletedPilot: [null,],
    })
    return LLIFfProjectForm
  }

}
