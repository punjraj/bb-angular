import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ServiceUserComponent } from './service-user.component';

describe('ServiceUserComponent', () => {
  let component: ServiceUserComponent;
  let fixture: ComponentFixture<ServiceUserComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
