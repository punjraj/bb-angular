import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTabsModule} from '@angular/material/tabs';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { ServiceUserRoutingModule } from './service-user-routing.module';
import { ServiceUserComponent } from './service-user.component';
import { ServiceUserService } from './service-user.service';
import { EditServiceUserComponent } from './edit-service-user/edit-service-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectSearchModule } from '../shared/components/mat-select-search/mat-select-search.module';
import { AppConfirmModule } from '../../framework/components/app-confirm/app-confirm.module';
import { FeatureAllowModule } from '../../framework/directives/features-allow.module';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { AppConfirmService } from '../../framework/components/app-confirm/app-confirm.service';
import { ManageUsersService } from '../manage-users/manage-users.service';
import { ServiceUserNavigation } from './service-user-nav';
import { ViewUserProfileComponent } from './view-service-user/view-user-profile/view-user-profile.component';
import { CompleteAssessmentsComponent } from './complete-assessments/complete-assessments.component';
import { CompleteAssessmentComponent } from './complete-assessments/complete-assessment/complete-assessment.component';
import { CompleteAssessmentsService } from "./complete-assessments/complete-assessments.service";
import { ViewCompleteAssessmentComponent } from "./complete-assessments/view-assessment/view-assessment.component";

import { PlanContentCardComponent } from './view-service-user/plan-content-card/plan-content-card.component';
import { PlanLearnerInfoComponent } from './view-service-user/learner-info/learner-info.component';
import { MaterialModule } from '../../../app/framework/material/material.module';
import { SharedModule } from '../../../app/framework/shared/shared.module';
import { ViewUserCvComponent } from './view-service-user/view-user-cv/view-user-cv.component';
import { DocumentsComponent } from './documents/documents.component';
import { UploadDocumentComponent } from './documents/upload-document/upload-document.component';
import { DocumentsService } from './documents/documents.service';
import { FileUploadModule } from '../shared/components/file-upload/file-upload.module';
import { MeetingListComponent } from './meet/meeting-list/meeting-list.component';
import { MeetingVideoComponent } from './meet/meeting-video/meeting-video.component';
import { MeetComponent } from './meet/meet.component';
import { VideoPlayerModule } from '../shared/components/video-player/video-player.module';
import { FavouritesJobsComponent } from './job-activity/favourites-jobs/favourites-jobs.component';
import { ViewFavouritesJobsComponent } from './job-activity/view-favourites-job/view-favourites-jobs/view-favourites-jobs.component';
import { JobApplicationsComponent } from './job-activity/job-applications/job-applications.component';
import { JobActivityComponent } from './job-activity/job-activity.component';
import { LocalJobsComponent } from './job-activity/local-jobs/local-jobs.component';
import { CompleteAssessmentsTabsComponent } from './complete-assessments/complete-assessments-tabs/complete-assessments-tabs.component';
import { PlanInductionComponent } from './view-service-user/plan-induction/plan-induction.component';
import { ScheduleTabComponent } from './view-service-user/plan-content-card/schedule-tab/schedule-tab.component';
import { TrackTabComponent } from './view-service-user/plan-content-card/track-tab/track-tab.component';
import { 
  SelectAnActivityModalComponent 
} from './view-service-user/plan-content-card/track-tab/f-structure-modals/select-an-activity-modal/select-an-activity-modal.component';
import { MatRadioModule } from '@angular/material/radio';
import { 
  AddInterventionModalComponent 
} from './view-service-user/plan-content-card/track-tab/f-structure-modals/add-intervention-modal/add-intervention-modal.component';
import { TrackTabService } from './view-service-user/plan-content-card/track-tab/track-tab.service';
import { 
  AddOrEditAppointmentModalComponent 
} from './view-service-user/plan-content-card/track-tab/f-structure-modals/add-or-edit-appointment-modal/add-or-edit-appointment-modal.component';
import { AddCommentModalComponent } from './view-service-user/plan-content-card/track-tab/f-structure-modals/add-comment-modal/add-comment-modal.component';
import { EntryInductionComponent } from './view-service-user/plan-content-card/track-tab/f-structure-entries/entry-induction/entry-induction.component';
import { EntryInterventionComponent } from './view-service-user/plan-content-card/track-tab/f-structure-entries/entry-intervention/entry-intervention.component';
import { EntryAppointmentComponent } from './view-service-user/plan-content-card/track-tab/f-structure-entries/entry-appointment/entry-appointment.component';
import { EntryCommentComponent } from './view-service-user/plan-content-card/track-tab/f-structure-entries/entry-comment/entry-comment.component';
import { EntrySystemComponent } from './view-service-user/plan-content-card/track-tab/f-structure-entries/entry-system/entry-system.component';
import { EditInterventionModalComponent } from './view-service-user/plan-content-card/track-tab/f-structure-modals/edit-intervention-modal/edit-intervention-modal.component';
import { FilterModule } from '../shared/components/filter/filter.module';
import { ProfileTabsComponent } from './view-service-user/profile-tabs/profile-tabs.component';
import { AssessmentRoutingModule } from './view-service-user/assessment/assessment-routing.module';

@NgModule({
  declarations: [ServiceUserComponent, 
    EditServiceUserComponent, 
    PlanLearnerInfoComponent,
    ViewUserProfileComponent,
    CompleteAssessmentsComponent,
    CompleteAssessmentComponent,
    PlanContentCardComponent,
    PlanLearnerInfoComponent,
    ViewCompleteAssessmentComponent,
    ViewUserCvComponent,
    DocumentsComponent,
    UploadDocumentComponent,
    MeetComponent,
    MeetingListComponent,
    MeetingVideoComponent,
    FavouritesJobsComponent,
    ViewFavouritesJobsComponent,
    JobApplicationsComponent,
    JobActivityComponent,
    LocalJobsComponent,
    CompleteAssessmentsTabsComponent,
    PlanInductionComponent,
    ScheduleTabComponent,
    TrackTabComponent,
    SelectAnActivityModalComponent,
    AddInterventionModalComponent,
    AddOrEditAppointmentModalComponent,
    AddCommentModalComponent,
    EntryInductionComponent,
    EntryInterventionComponent,
    EntryAppointmentComponent,
    EntryCommentComponent,
    EntrySystemComponent,
    EditInterventionModalComponent,
    ProfileTabsComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ServiceUserRoutingModule,
    ReactiveFormsModule,
    MatSelectSearchModule,
    AppConfirmModule,
    FeatureAllowModule,
    MatTabsModule,
    MatButtonToggleModule,
    SharedModule,
    FileUploadModule,
    VideoPlayerModule,
    MatRadioModule,
    FilterModule,
    AssessmentRoutingModule
  ],
  providers: [
    ServiceUserService,
    ManageUsersService,
    SnackBarService,
    AppConfirmService,
    ServiceUserNavigation,
    CompleteAssessmentsService,
    DocumentsService,
    TrackTabService
  ],
  entryComponents: [
    SelectAnActivityModalComponent,
    AddInterventionModalComponent,
    AddOrEditAppointmentModalComponent,
    AddCommentModalComponent,
    EditInterventionModalComponent,
  ]
})
export class ServiceUserModule { }
