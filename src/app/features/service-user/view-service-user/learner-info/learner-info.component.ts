import { Component, OnInit } from '@angular/core';
import { ServiceUserService } from '../../service-user.service';
import { Router,ActivatedRoute } from '@angular/router';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';

@Component({
  selector: 'vc-plan-learner-info',
  templateUrl: './learner-info.component.html',
  styleUrls: ['./learner-info.component.scss']
})
export class PlanLearnerInfoComponent implements OnInit {

  learner: any;
  courseSize = 0;
  lddSize = 0;

  constructor(
    private readonly planService: ServiceUserService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly snackBarService: SnackBarService) { 
  }

  ngOnInit() {
    this.resolveUserName();
  }
  resolveUserName() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.id) {
        this.planService.getProfileDetails(params.id).subscribe(
          response => {
            this.learner = response;
            this.courseSize = this.learner.courseList.length;
             this.lddSize = this.learner.lldAndHealthProblemList.length;
          }, error => {
             this.snackBarService.error(`${error.error.applicationMessage}`);
             this.router.navigate(['./plan']);
          }
        );
      }
    });
  }

}
