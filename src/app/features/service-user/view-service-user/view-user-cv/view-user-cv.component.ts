import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ServiceUserService } from './../../service-user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceUserNavigation } from './../../service-user-nav';
import { Component, OnInit, ViewChild } from '@angular/core';
import { InPageNavService } from '../../../shared/components/in-page-nav/in-page-nav.service';

@Component({
  selector: 'app-view-user-cv',
  templateUrl: './view-user-cv.component.html',
  styleUrls: ['./view-user-cv.component.scss']
})
export class ViewUserCvComponent implements OnInit {

  fname: string;
  lname: string;
  prn: string;
  allCVDetails: any;
  id: any;
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, {static:false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort: MatSort;
  constructor(
    private readonly inPageNavService: InPageNavService,
    private readonly serviceUserNavigation: ServiceUserNavigation,
    private readonly route: ActivatedRoute,
    private readonly serviceUserService: ServiceUserService,
    private readonly snack: MatSnackBar,
    private readonly router: Router
  ) {

    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();

  }

 setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.fullName) {
        this.fname = params.fullName;
        this.id = params.id;
      }
      this.route.snapshot.data.title = `${this.fname}`;
    });
  }

  ngOnInit() {
    this.getAllAssessments(this.id);
  }


  getAllAssessments(suID) {
    this.serviceUserService.getCVDetails(suID).subscribe(
      response => {
        this.dataSource.data = response;
        console.log(this.dataSource);

        this.paginator.length = response.length;
        this.dataSource.paginator = this.paginator;

      },
      error => this.snack.open(error.errorMessage, 'Dismiss', { duration: 4000 })
    );
  }

  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  download(id) {
    this.serviceUserService.download(id).subscribe(res => {
      console.log("done");
    });
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }

  onExitClicked() {
    this.router.navigate(['./service-user']);
  }


}
