import { Component, OnInit } from '@angular/core';
import { InPageNavService } from 'src/app/features/shared/components/in-page-nav/in-page-nav.service';
import { ServiceUserNavigation } from '../../service-user-nav';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-assessment',
    templateUrl: './assessment.component.html',
    styleUrls: ['./assessment.component.scss']
  })
  export class AssessmentComponent  {
    fname: string;
    
    constructor(
        private readonly inPageNavService: InPageNavService,
        private readonly route: ActivatedRoute,
        private readonly serviceUserNavigation: ServiceUserNavigation
        
    ) {
        this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
        this.setTitle();
    }

    setTitle() {
        this.route.queryParams.subscribe((params: any) => {
          if (params.fullName) {
            this.fname = params.fullName;
          }
          this.route.snapshot.data['title'] = `${this.fname}`;
        });
    }

    ngOnDestroy() {
        this.inPageNavService.setNavItems(null);
    }
    questionDisplay() {
        
    }

}