import { NgModule } from '@angular/core';
import { AssessmentRoutingModule } from './assessment-routing.module';
import { AssessmentService } from './assessment.service';
import { MaterialModule } from 'src/app/framework/material/material.module';
import { AssessmentComponent } from './assessment.component';

@NgModule({
    declarations: [
        AssessmentComponent,
    ],
    imports:[
        MaterialModule,
        AssessmentRoutingModule
    ],
    providers: [AssessmentService]
})
export class AssessmentModule {

}