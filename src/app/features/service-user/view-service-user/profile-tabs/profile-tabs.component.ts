import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-tabs',
  templateUrl: './profile-tabs.component.html',
  styleUrls: ['./profile-tabs.component.scss']
})
export class ProfileTabsComponent implements OnInit {
  viewUserProfile: boolean = false;
  newAssessment: boolean = false;
  fname: string;
  userId: any;
  constructor(
    private readonly route: ActivatedRoute,
  ) { 
    this.setTitle();
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.fullName) {
        this.fname = params.fullName;
        this.userId = params.id;
      }
      this.route.snapshot.data['title'] = `${this.fname}`;
    });
  }

  ngOnInit() {
     
  }
  profileTabs(tabName: string) {
    switch (tabName) {
      case 'profile':
        this.viewUserProfile = true;
        this.newAssessment = false;
        break;
      case 'new-assessment':
        this.viewUserProfile = false;
        this.newAssessment = true;
        break;
    }
  }

  
}
