import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseUrl } from './../../../../framework/constants/url-constants';

@Injectable({
  providedIn: 'root'
})
export class PlanInductionService {

  constructor(private readonly http: HttpClient) { }

  getPlanInductionDetails(userId) {
    const href = `${BaseUrl.PLAN}/induction-Plan/details?planTypeIdentifier=1&userId=${userId}`;
    return this.http.get<any>(href);
  }

  getPlanInductionHistory(sectionIdentifier, userId) {
    const href = `${BaseUrl.PLAN}/induction-history?planTypeIdentifier=1&sectionIdentifier=${sectionIdentifier}&userId=${userId}`;
    return this.http.get<any>(href);
  }

}
