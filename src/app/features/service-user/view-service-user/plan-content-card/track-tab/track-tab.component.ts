import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SelectAnActivityModalComponent } from './f-structure-modals/select-an-activity-modal/select-an-activity-modal.component';
import { AddInterventionModalComponent } from './f-structure-modals/add-intervention-modal/add-intervention-modal.component';
import { SnackBarService } from '../../../../../framework/service/snack-bar.service';
import { TrackTabService } from './track-tab.service';
import { ActivatedRoute } from '@angular/router';
import { Utility } from '../../../../../framework/utils/utility';
import { AddOrEditAppointmentModalComponent } from './f-structure-modals/add-or-edit-appointment-modal/add-or-edit-appointment-modal.component';
import { AddCommentModalComponent } from './f-structure-modals/add-comment-modal/add-comment-modal.component';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-track-tab',
  templateUrl: './track-tab.component.html',
  styleUrls: ['./track-tab.component.scss']
})
export class TrackTabComponent implements OnInit {
  @Input() fStructureData: any;
  @Input() loadMoreVisible: boolean;
  @Output() loadMore = new EventEmitter();
  @Output() refreshFStructureData = new EventEmitter();
  @Output() emitUpdateInterventionEvent= new EventEmitter();

  activityRefData: any;
  serviceUserId: number;
  currentDate = new Date(new Date().setHours(0, 0, 0, 0));
  workers: any;
  interventions: any;

  constructor(
    public  dialog: MatDialog,
    private readonly snackBarService: SnackBarService,
    private readonly trackTabService: TrackTabService,
    private readonly route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.resolveServiceUserId();
    this.resolveActivityRefData();
  }

  resolveServiceUserId() {
    this.route.queryParams.subscribe(params => {
      this.serviceUserId = parseInt(params['id']);
    });
  }

  resolveActivityRefData() {
    this.trackTabService.getActivityRefData().subscribe(data => {
      this.activityRefData = data;
    })
  }

  addEntry() {
    this.openSelectAnActivityModal();
  }

  openSelectAnActivityModal(): void {
    const dialogRef = this.dialog.open(SelectAnActivityModalComponent, {
      disableClose: true,
      autoFocus: false,
      height: '300px',
      width: '450px',
      data: this.activityRefData.activityList
    });
    dialogRef.afterClosed().subscribe(selectedActivity => {
      switch(selectedActivity.identifier) {
        case 'INT-10':
          this.openAddInterventionModal();
          break;
        case 'APPT-10':
          forkJoin(this.trackTabService.getInterventionList(this.serviceUserId), this.trackTabService.getUserList()).subscribe(response => {
            this.interventions = response[0];
            this.workers = response[1].filter(worker => worker.isActive === true);
            this.openAddAppointmentModal();
          });
          break;
        case 'CMT-10':
          this.openAddCommentModal();
          break;
        default:
          break;
      }
    });
  }

  openAddInterventionModal(): void {
    const dialogRef = this.dialog.open(AddInterventionModalComponent, {
      disableClose: true,
      autoFocus: false,
      data: {
        pathwayList: this.activityRefData.pathwayList,
        organisationList: this.activityRefData.organizationList,
        interventionList: this.activityRefData.interventionList,
        projectList: this.activityRefData.projectList,
      }
    });

    dialogRef.afterClosed().subscribe(addInterventionPayload => {
      if (addInterventionPayload) {
        const formattedPayload = {
          "activityId": this.activityRefData.activityList.filter(activity => activity.name === 'Intervention')[0].id,
          "id": 0,
          "refInterventionId": addInterventionPayload.interventionName.id,
          "otherIntName": addInterventionPayload.otherInterventionName,
          "pathwayId": addInterventionPayload.pathway.id,
          "plannedEndDate": Utility.transformDateToString(addInterventionPayload.plannedEndDate),
          "serviceUserId": this.serviceUserId,
          "startDate": Utility.transformDateToString(addInterventionPayload.startDate),
          "organizationId": addInterventionPayload.orgDeliveringInter.id,
          "otherOrganization": addInterventionPayload.organisationName,
          "contactName": addInterventionPayload.contactName,
          "telephone": addInterventionPayload.telephone,
          "email": addInterventionPayload.email,
          "description": addInterventionPayload.interventionDescription,
          "projectId": addInterventionPayload.project.id,
          "otherProject": addInterventionPayload.otherProject,
        }
        this.trackTabService.createIntervention(formattedPayload).subscribe(response => {
          this.snackBarService.success(response.message.successMessage);
          this.refreshFStructureData.emit();
        }, error => {
          this.snackBarService.error(error.error.applicationMessage);
        })
      }
    });
  }
  
  formatAppointmentTime(timeHours, timeMinutes) {
    const timeHoursString = `${timeHours}`;
    if (timeHoursString.length === 1) {
      return `0${timeHours}:${timeMinutes}`
    } else {
      return `${timeHours}:${timeMinutes}`
    }
  }

  openAddAppointmentModal(): void {
    const dialogRef = this.dialog.open(AddOrEditAppointmentModalComponent, {
      disableClose: true,
      autoFocus: false,
      data: {
        appointmentId: null,
        appointmentList: this.activityRefData.appointmentList,
        pathwayList: this.activityRefData.pathwayList,
        statusList: this.activityRefData.statusList.filter(status => 
          status.activityId === this.activityRefData.activityList.filter(activity => activity.name === 'Appointment')[0].id
        ),
        workers: this.workers,
        interventions: this.interventions,
      }
    });
    dialogRef.afterClosed().subscribe(addAppointmentData => {
      if (addAppointmentData) {
        const formattedPayload = {
          "activityId": this.activityRefData.activityList.filter(activity => activity.name === 'Appointment')[0].id,
          "appointmentId": addAppointmentData.formValues.appointmentType.id,
          "id": 0,
          "interventionId": addAppointmentData.formValues.interventionLink ? addAppointmentData.formValues.interventionLink.id : null,
          "mdate": Utility.transformDateToString(addAppointmentData.formValues.date),
          "mtime": this.formatAppointmentTime(addAppointmentData.formValues.timeHours, addAppointmentData.formValues.timeMinutes),
          "notes": addAppointmentData.isAppointmentInPast ? addAppointmentData.formValues.meetingNotes : null,
          "other": addAppointmentData.formValues.appointmentType.id === this.activityRefData.appointmentList.filter(appointment => appointment.name === 'Other')[0].id ? 
            addAppointmentData.formValues.otherAppointmentName : null,
          "serviceUserId": this.serviceUserId,
          "staffId": addAppointmentData.formValues.worker.id,
          "statusId": addAppointmentData.isAppointmentInPast ? addAppointmentData.formValues.attendance.id : null​​​​
        }
        this.trackTabService.createMeeting(formattedPayload).subscribe(response => {
          this.snackBarService.success(response.message.successMessage);
          this.refreshFStructureData.emit();
        }, error => {
          this.snackBarService.error(error.error.applicationMessage);
        })
      }
    });
  }

  openAddCommentModal(): void {
    const dialogRef = this.dialog.open(AddCommentModalComponent, {
      disableClose: true,
      autoFocus: false,
      width: '550px',
    });
    dialogRef.afterClosed().subscribe(addCommentPayload => {
      if (addCommentPayload) {
        const formattedPayload = {
          "activityId": this.activityRefData.activityList.filter(activity => activity.name === 'Comment')[0].id,
          "id": 0,
          "notes": addCommentPayload.comment,
          "serviceUserId": this.serviceUserId,
        }
        this.trackTabService.createComment(formattedPayload).subscribe(response => {
          this.snackBarService.success(response.message.successMessage);
          this.refreshFStructureData.emit();
        }, error => {
          this.snackBarService.error(error.error.applicationMessage);
        })
      }
    });
  }

  setEntryMarginBottom(index) {
    if (index + 1 === this.fStructureData.length) {
      return '0px'
    } else {
      return '60px'
    }
  }

  setInductionEntryMarginBottom(index) {
    if (
      this.fStructureData[index + 1] && 
      this.fStructureData[index + 1].activityId === -1 && 
      this.fStructureData[index + 1].entryDateUnix === this.fStructureData[index].entryDateUnix
    ) {  
      return '-10px'
    } else if (index + 1 === this.fStructureData.length) {
      return '0px'
    } else {
      return '60px'
    }
  }

  decideIfShouldShowInductionIcon(index) {
    if (
      this.fStructureData[index - 1].activityId === -1 && 
      this.fStructureData[index - 1].entryDateUnix === this.fStructureData[index].entryDateUnix
    ) {
      return 'none'
    } else {
      return 'flex'
    }
  }

  setFLineHeight(index) {
    if (index + 1 === this.fStructureData.length && !this.loadMoreVisible) {
      return '0px'
    } else {
      if (this.fStructureData[index].activityId === -1) {
        let allInductionEntriesFlag = true;
        for (let i = 1; i < this.fStructureData.length; i++) {
          if (this.fStructureData[i].activityId !== -1) {
            allInductionEntriesFlag = false;
          }
        }
        return this.setFLineHeightIfAllEntriesAreInduction(allInductionEntriesFlag)
      }
      return 'auto'
    }
  }

  setFLineHeightIfAllEntriesAreInduction(allInductionEntriesFlag) {
    if (allInductionEntriesFlag) {
      return '0px'
    } else {
      return 'auto'
    }
  }

  onClickLoadMore() {
    this.loadMore.emit();
  }

  getFilteredStatusList() {
    if (this.activityRefData) {
      return this.activityRefData.statusList.filter(status => status.activityId === this.activityRefData.activityList.filter(activity => activity.name === 'Intervention')[0].id)
    } else {
      return []
    }
  }

  onUpdateEntry(event) {
    this.emitUpdateInterventionEvent.emit('event')
  }

  shouldShowTodayLineBrackets(i) {
    if (i  < this.fStructureData.length - 1) {
      if (this.fStructureData[i + 1].entryDateUnix === this.fStructureData[i].entryDateUnix) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  getActivityId(activityName) {
    if (this.activityRefData) {
      return this.activityRefData.activityList.filter(activity => activity.name === activityName)[0].id
    } else {
      return undefined
    }
  }
}
