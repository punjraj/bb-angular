import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { AppDateAdapter, APP_DATE_FORMATS } from './../../../../../../../framework/components/date-adapter/date-adapter';
import { TrackTabService } from '../../track-tab.service';
import { AddInterventionModalComponent } from '../add-intervention-modal/add-intervention-modal.component';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-intervention-modal',
  templateUrl: './edit-intervention-modal.component.html',
  styleUrls: ['./edit-intervention-modal.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ]
})
export class EditInterventionModalComponent implements OnInit {

  editInterventionForm: FormGroup;
  outcomeIdValidators = [];
  withdrawalIdValidators = [];

  constructor(
    private readonly fb: FormBuilder,
    private readonly trackTabService: TrackTabService,
    public dialogRef: MatDialogRef<AddInterventionModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
    this.initForm()
    this.initConditionalFormValidators()
    this.resolveInterventionData()
  }

  resolveInterventionData() {
    this.trackTabService.getSingleInterventionData(this.data.interventionId).subscribe(data => {
      this.editInterventionForm.patchValue(data);
      this.editInterventionForm.get('interventionName').setValue((data.otherIntName) ? data.otherIntName : data.interventionName);
      this.editInterventionForm.get('organizationName').setValue((data.otherOrganization) ? data.otherOrganization : data.organizationName);
      this.editInterventionForm.get('projectName').setValue((data.otherProject) ? data.otherProject : data.projectName);
      this.editInterventionForm.get('interventionName').disable();
      this.editInterventionForm.get('pathwayName').disable();
      this.editInterventionForm.get('startDate').disable();
      this.editInterventionForm.get('plannedEndDate').disable();
      this.editInterventionForm.get('organizationName').disable();
      this.editInterventionForm.get('contactName').disable();
      this.editInterventionForm.get('telephone').disable();
      this.editInterventionForm.get('email').disable();
      this.editInterventionForm.get('description').disable();
      this.editInterventionForm.get('projectName').disable();
      
    })
  }

  initForm() {
    this.editInterventionForm = this.fb.group({
      activityId: [null],
      actualEndDate: [null, [Validators.required]],
      id: [null],
      refInterventionId: [null],
      interventionName: [this.data.interventionName],
      outcomeId: [null, this.outcomeIdValidators],
      pathwayId: [null],
      plannedEndDate: [null],
      serviceUserId: [null],
      startDate: [null],
      statusId: [null, [Validators.required]],
      pathwayName: [this.data.pathwayName],
      organizationId: [null],
      organizationName: [null],
      contactName: [null],
      telephone: [null],
      email: [null],
      description: [null],
      withdrawalId: [null, this.outcomeIdValidators],
      projectId:[null],
      projectName: [null]
    });
  }

  initConditionalFormValidators() {
    this.editInterventionForm.get('statusId').valueChanges.subscribe(value => {
      if (value === 25) {
        this.editInterventionForm.controls.outcomeId.setValidators(this.outcomeIdValidators.concat(Validators.required))
      } else if (value === 26) {
        this.editInterventionForm.controls.withdrawalId.setValidators(this.withdrawalIdValidators.concat(Validators.required));
      } else {
        this.editInterventionForm.controls.outcomeId.setValue(null)
        this.editInterventionForm.controls.outcomeId.setErrors(null);
        this.editInterventionForm.controls.outcomeId.markAsPristine();
      }
    })
  };

  confirmEditIntervention() {
    this.editInterventionForm.get('interventionName').enable();
    this.editInterventionForm.get('pathwayName').enable();
    this.editInterventionForm.get('startDate').enable();
    this.editInterventionForm.get('plannedEndDate').enable();
    this.editInterventionForm.get('organizationName').enable();
    this.editInterventionForm.get('contactName').enable();
    this.editInterventionForm.get('telephone').enable();
    this.editInterventionForm.get('email').enable();
    this.editInterventionForm.get('description').enable();
    this.editInterventionForm.get('projectName').enable();
    this.dialogRef.close(this.editInterventionForm.value);
  }

  cancelEditIntervention() {
    this.dialogRef.close();
  }

  calculateMinActualEndDate() {
    if (this.editInterventionForm.controls.startDate.value) {
      return moment(this.editInterventionForm.controls.startDate.value).add(1, 'days');
    } else {
      return null
    }
  }

}
