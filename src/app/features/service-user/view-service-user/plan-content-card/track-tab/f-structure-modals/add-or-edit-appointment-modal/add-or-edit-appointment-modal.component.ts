import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { AppDateAdapter, APP_DATE_FORMATS } from '../../../../../../../framework/components/date-adapter/date-adapter';
import * as moment from 'moment';
import { TrackTabService } from '../../track-tab.service';

@Component({
  selector: 'app-add-or-edit-appointment-modal',
  templateUrl: './add-or-edit-appointment-modal.component.html',
  styleUrls: ['./add-or-edit-appointment-modal.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ]
})
export class AddOrEditAppointmentModalComponent implements OnInit {
  addOrUpdateAppointmentForm: FormGroup;
  currentDate = new Date();
  hours = [
    { value: 0, label: '00' }, { value: 1, label: '01' }, { value: 2, label: '02' }, { value: 3, label: '03' }, { value: 4, label: '04' }, { value: 5, label: '05' }, 
    { value: 6, label: '06' }, { value: 7, label: '07' }, { value: 8, label: '08' }, { value: 9, label: '09' }, { value: 10, label: '10' }, { value: 11, label: '11' }, 
    { value: 12, label: '12' }, { value: 13, label: '13' }, { value: 14, label: '14' }, { value: 15, label: '15' }, { value: 16, label: '16' }, { value: 17, label: '17' }, 
    { value: 18, label: '18' }, { value: 19, label: '19' }, { value: 20, label: '20' }, { value: 21, label: '21' }, { value: 22, label: '22' }, { value: 23, label: '23' }, 
  ];
  minutes = ['00', '15', '30', '45'];
  meetingNotesValidators = [Validators.maxLength(1000)];
  attendanceValidators = [];
  otherAppointmentNameValidators = [Validators.maxLength(150)];
  pastAppointment = false;

  constructor(
    private readonly fb: FormBuilder,
    public dialogRef: MatDialogRef<AddOrEditAppointmentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private readonly trackTabService: TrackTabService
  ) { }

  ngOnInit() {
    this.initForm();
    this.initConditionalFormValidators();
    this.data.interventions.forEach(e => {
      e.name = e.name.charAt(0).toUpperCase() + e.name.slice(1);
    });
    if (this.data.appointmentId) {
      this.resolveAppointmentData()
    }
  }

  initForm() {
    this.addOrUpdateAppointmentForm = this.fb.group({
      appointmentType: [null, [Validators.required]],
      worker: [null, [Validators.required]],
      date: [null, [Validators.required]],
      timeHours: [null, [Validators.required]],
      timeMinutes: [null, [Validators.required]],
      interventionLink: [null],
      meetingNotes: [null, this.meetingNotesValidators],
      attendance: [null, this.attendanceValidators],
      otherAppointmentName: [null, this.otherAppointmentNameValidators]
    });
  }

  resolveAppointmentData() {
    this.trackTabService.getSingleAppointmentData(this.data.appointmentId).subscribe(appointmentData => {
      const appointmentTypeIndex = this.data.appointmentList.findIndex(appointment => appointment.id === appointmentData.appointmentId)
      this.addOrUpdateAppointmentForm.controls.appointmentType.setValue(this.data.appointmentList[appointmentTypeIndex]);
      
      const workerIndex = this.data.workers.findIndex(worker => worker.id === appointmentData.staffId)
      this.addOrUpdateAppointmentForm.controls.worker.setValue(this.data.workers[workerIndex]);
      
      this.addOrUpdateAppointmentForm.controls.date.setValue(moment(appointmentData.mdate));
      
      const meetingTimeSplitArr = appointmentData.mtime.split(':')
      
      const hoursArrIndexOfAppointment = this.hours.findIndex(hour => hour.label === meetingTimeSplitArr[0])
      this.addOrUpdateAppointmentForm.controls.timeHours.patchValue(this.hours[hoursArrIndexOfAppointment].value);

      const minutesArrIndexOfAppointment = this.minutes.findIndex(minute => minute === meetingTimeSplitArr[1])
      this.addOrUpdateAppointmentForm.controls.timeMinutes.patchValue(this.minutes[minutesArrIndexOfAppointment]);

      if (appointmentData.interventionId) {
        const interventionIndex = this.data.interventions.findIndex(intervention => intervention.id === appointmentData.interventionId)
        this.addOrUpdateAppointmentForm.controls.interventionLink.setValue(this.data.interventions[interventionIndex]);
      }

      if (appointmentData.notes) {
        this.addOrUpdateAppointmentForm.controls.meetingNotes.setValue(appointmentData.notes);
      }

      if (appointmentData.statusId) {
        const statusIndex = this.data.statusList.findIndex(status => status.id === appointmentData.statusId)
        this.addOrUpdateAppointmentForm.controls.attendance.setValue(this.data.statusList[statusIndex]);
      }

      if (appointmentData.appointmentId === 47 && appointmentData.other) {
        this.addOrUpdateAppointmentForm.controls.otherAppointmentName.setValue(appointmentData.other);
      }
    })
  }

  initConditionalFormValidators() {
    this.addOrUpdateAppointmentForm.get('appointmentType').valueChanges.subscribe(value => {
      if (value.identifier === 'OTH-10') {
        this.addOrUpdateAppointmentForm.controls.otherAppointmentName.setValidators(this.otherAppointmentNameValidators.concat(Validators.required))
      } else {
        this.addOrUpdateAppointmentForm.controls.otherAppointmentName.setValue(null)
        this.addOrUpdateAppointmentForm.controls.otherAppointmentName.setErrors(null);
        this.addOrUpdateAppointmentForm.controls.otherAppointmentName.markAsPristine();
      }
    })
    this.addOrUpdateAppointmentForm.get('date').valueChanges.subscribe(value => {
      this.checkAppointment();
    })
    this.addOrUpdateAppointmentForm.get('timeHours').valueChanges.subscribe(value => {
      this.checkAppointment();
    })
    this.addOrUpdateAppointmentForm.get('timeMinutes').valueChanges.subscribe(value => {
      this.checkAppointment();
    })
  };

   checkAppointment() {
    if (this.isAppointmentInPast()) {
      this.addOrUpdateAppointmentForm.controls.meetingNotes.setValidators(this.meetingNotesValidators.concat(Validators.required));
      this.addOrUpdateAppointmentForm.controls.attendance.setValidators(this.attendanceValidators.concat(Validators.required));
    }
    else {
      this.resetMeetingNotesValidation();
      this.resetAttendanceValidation();
    }
  }

  resetMeetingNotesValidation() {
    this.meetingNotesValidators = [Validators.maxLength(1000)]
    this.addOrUpdateAppointmentForm.controls.meetingNotes.clearValidators()
    this.addOrUpdateAppointmentForm.controls.meetingNotes.setValidators(this.meetingNotesValidators)
    this.addOrUpdateAppointmentForm.controls.meetingNotes.setValue(null)
    this.addOrUpdateAppointmentForm.controls.meetingNotes.setErrors(null);
    this.addOrUpdateAppointmentForm.controls.meetingNotes.markAsPristine();
    this.addOrUpdateAppointmentForm.updateValueAndValidity();
  }

  resetAttendanceValidation() {
    this.attendanceValidators = [Validators.maxLength(150)]
    this.addOrUpdateAppointmentForm.controls.attendance.clearValidators()
    this.addOrUpdateAppointmentForm.controls.attendance.setValidators(this.attendanceValidators)
    this.addOrUpdateAppointmentForm.controls.attendance.setValue(null)
    this.addOrUpdateAppointmentForm.controls.attendance.setErrors(null);
    this.addOrUpdateAppointmentForm.controls.attendance.markAsPristine();
    this.addOrUpdateAppointmentForm.updateValueAndValidity();
  }

  confirmAddAppointment() {
    const appointmentData = {
      formValues: this.addOrUpdateAppointmentForm.value,
      isAppointmentInPast: this.isAppointmentInPast(),
    }
    this.dialogRef.close(appointmentData);
  }

  cancelAddAppointment() {
    this.dialogRef.close();
  }

  isAppointmentInPast() {
    if (
      this.addOrUpdateAppointmentForm.controls.date.value && 
      (this.addOrUpdateAppointmentForm.controls.timeHours.value || this.addOrUpdateAppointmentForm.controls.timeHours.value === 0) && 
      (this.addOrUpdateAppointmentForm.controls.timeMinutes.value || this.addOrUpdateAppointmentForm.controls.timeMinutes.value === '00')
    ) {
      const isCurrentDate = this.addOrUpdateAppointmentForm.controls.date.value.isSame(new Date(), "day");
      if (isCurrentDate && this.addOrUpdateAppointmentForm.controls.timeHours.value && this.addOrUpdateAppointmentForm.controls.timeMinutes.value) {
        return this.setIsPastAppointment();
      } else if (this.addOrUpdateAppointmentForm.controls.date.value < this.currentDate) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }

  setIsPastAppointment() {
    const appointmentYear = this.addOrUpdateAppointmentForm.controls.date.value._i.year || moment(this.addOrUpdateAppointmentForm.controls.date.value).year()
    const appointmentMonth = this.addOrUpdateAppointmentForm.controls.date.value._i.month || moment(this.addOrUpdateAppointmentForm.controls.date.value).month()
    const appointmentDay = this.addOrUpdateAppointmentForm.controls.date.value._i.date || moment(this.addOrUpdateAppointmentForm.controls.date.value).date()
    const appointmentHours = this.addOrUpdateAppointmentForm.controls.timeHours.value === 0 ? '00' : this.addOrUpdateAppointmentForm.controls.timeHours.value
    const appointmentMinutes = this.addOrUpdateAppointmentForm.controls.timeMinutes.value
    const appointmentDateAndTime = moment(`${appointmentYear}-${appointmentMonth + 1}-${appointmentDay} ${appointmentHours}:${appointmentMinutes}`)
    this.pastAppointment = appointmentDateAndTime.isBefore()
    if (this.pastAppointment) {
      return true
    } else {
      return false
    }
  }

}
