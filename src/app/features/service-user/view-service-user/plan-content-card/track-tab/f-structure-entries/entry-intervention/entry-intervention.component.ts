import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SnackBarService } from './../../../../../../../framework/service/snack-bar.service';
import { Utility } from './../../../../../../../framework/utils/utility';
import { EditInterventionModalComponent } from '../../f-structure-modals/edit-intervention-modal/edit-intervention-modal.component';
import { TrackTabService } from '../../track-tab.service';

@Component({
  selector: 'app-entry-intervention',
  templateUrl: './entry-intervention.component.html',
  styleUrls: ['./entry-intervention.component.scss']
})
export class EntryInterventionComponent implements OnInit {
  @Input() entryData: any;
  @Input() statusList: any;
  @Input() outcomeList: any;
  @Input() withdrawalList: any;
  dateSeparator = ' - '
  textPrimaryHex = '#000000';
  textSecondaryHex = '#505a5f';
  @Output() handleUpdateInterventionEntry = new EventEmitter

  constructor(
    public dialog: MatDialog,
    private readonly trackTabService: TrackTabService,
    private readonly snackBarService: SnackBarService,
  ) { }

  ngOnInit() {
    this.formatTitle()
    this.formatInterventionDates()
  }

  formatTitle() {
    this.entryData.title = this.entryData.title.charAt(0).toUpperCase() + this.entryData.title.slice(1);
  }

  formatInterventionDates() {
    const hyphenIndex = this.entryData.subTitle.indexOf('-')
    this.entryData.startDate = this.entryData.subTitle.substring(0, hyphenIndex - 1)
    this.entryData.endDate = this.entryData.subTitle.substring(hyphenIndex + 2)
  }


  setPathwayBackgroundColor(pathwayName) {
    switch(pathwayName) {
      case 'Education, Training and Employment':
        return '#F47738'
      case 'Finance':
        return '#28a197'
      case 'Health and Wellbeing':
        return '#5694CA'
      case 'Housing':
        return '#F499BE'
      case 'Drugs and Alcohol':
        return '#85994B'
      case 'Family':
        return '#6f72af'
      case 'Behaviour and Attitudes':
        return '#912b88'
      default:
        return '#EEEFEF'
    }
  }

  setDateFontColor(interventionType, dateType) {
    if (interventionType === 'Start') {
      switch(dateType) {
        case 'Start':
          return this.textPrimaryHex;
        case 'Separator':
          return this.textSecondaryHex;
        case 'End':
          return this.textSecondaryHex;
        default:
          return this.textPrimaryHex;
      }
    } else {
      switch(dateType) {
        case 'Start':
          return this.textSecondaryHex;
        case 'Separator':
          return this.textSecondaryHex;
        case 'End':
          return this.textPrimaryHex;
        default:
          return this.textPrimaryHex;
      }
    }
  }

  openEditInterventionModal() {
    const dialogRef = this.dialog.open(EditInterventionModalComponent, {
      disableClose: true,
      autoFocus: false,
      data: {
        interventionId: this.entryData.id,
        pathwayName: this.entryData.pathway,
        statusList: this.statusList,
        outcomeList: this.outcomeList,
        withdrawalList:this.withdrawalList
      }
    });
    dialogRef.afterClosed().subscribe(editInterventionPayload => {
      if (editInterventionPayload) {
        const formattedPayload = {
          ...editInterventionPayload,
          "otherIntName": editInterventionPayload.refInterventionId === 11 ? editInterventionPayload.interventionName : null,
          "otherOrganization": editInterventionPayload.organizationId === 6 ? editInterventionPayload.organizationName : null,
          "otherProject": editInterventionPayload.projectId === 13 ? editInterventionPayload.projectName : null,
          "startDate": Utility.transformDateToString(editInterventionPayload.startDate),
          "plannedEndDate": Utility.transformDateToString(editInterventionPayload.plannedEndDate),
          "actualEndDate": Utility.transformDateToString(editInterventionPayload.actualEndDate),
          "outcomeId": editInterventionPayload.statusId === 25 ? editInterventionPayload.outcomeId : null
        }
        this.trackTabService.updateSingleIntervention(formattedPayload).subscribe(response => {
          this.snackBarService.success(response.message.successMessage);
          this.handleUpdateInterventionEntry.emit()
        }, error => {
          this.snackBarService.error(error.error.applicationMessage);
        })
      }
    });
  }

}
