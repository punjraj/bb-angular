import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { SnackBarService } from './../../../../../../../framework/service/snack-bar.service';
import { Utility } from './../../../../../../../framework/utils/utility';
import { AddOrEditAppointmentModalComponent } from '../../f-structure-modals/add-or-edit-appointment-modal/add-or-edit-appointment-modal.component';
import { TrackTabService } from '../../track-tab.service';

@Component({
  selector: 'app-entry-appointment',
  templateUrl: './entry-appointment.component.html',
  styleUrls: ['./entry-appointment.component.scss']
})
export class EntryAppointmentComponent implements OnInit {
  @Input() entryData: any;
  @Input() activityRefData: any;
  interventions = undefined;
  workers = undefined;
  serviceUserId: number;
  @Output() handleUpdateAppointmentEntry = new EventEmitter

  constructor(
    public dialog: MatDialog,
    private readonly route: ActivatedRoute,
    private readonly trackTabService: TrackTabService,
    private readonly snackBarService: SnackBarService,
  ) { }

  ngOnInit() {
    this.resolveServiceUserId();
    this.formatTitle()
    this.formatSubtitle()
    this.formatInterventionName()
  }

  resolveServiceUserId() {
    this.route.queryParams.subscribe(params => {
      this.serviceUserId = parseInt(params['id']);
    });
  }

  formatTitle() {
    this.entryData.title = this.entryData.title.charAt(0).toUpperCase() + this.entryData.title.slice(1);
  }

  formatSubtitle() {
    const afterWithIndex = this.entryData.subTitle.indexOf("with");
    this.entryData.formattedSubtitle = this.entryData.subTitle.slice(0, afterWithIndex + 5)
    this.entryData.keyWorkerName = this.entryData.subTitle.slice(afterWithIndex + 5)
  }

  formatInterventionName() {
    if (this.entryData.interventionName) {
      this.entryData.interventionName = this.entryData.interventionName.charAt(0).toUpperCase() + this.entryData.interventionName.slice(1);
    }
  }

  setBulletColor(pathwayName) {
    switch(pathwayName) {
      case 'Education, Training and Employment':
        return '#F47738'
      case 'Finance':
        return '#28a197'
      case 'Health and Wellbeing':
        return '#5694CA'
      case 'Housing':
        return '#F499BE'
      case 'Drugs and Alcohol':
        return '#85994B'
      case 'Family':
        return '#6f72af'
      case 'Behaviour and Attitudes':
        return '#912b88'
      default:
        return '#EEEFEF'
    }
  }

  openAddOrEditAppointmentModal() {
    forkJoin(this.trackTabService.getInterventionList(this.serviceUserId), this.trackTabService.getUserList()).subscribe(response => {
      this.interventions = response[0];
      this.workers = response[1].filter(worker => worker.isActive === true);
      const dialogRef = this.dialog.open(AddOrEditAppointmentModalComponent, {
        disableClose: true,
        autoFocus: false,
        data: {
          appointmentId: this.entryData.id,
          appointmentList: this.activityRefData.appointmentList,
          pathwayList: this.activityRefData.pathwayList,
          statusList: this.activityRefData.statusList.filter(status => status.activityId === 27),
          workers: this.workers,
          interventions: this.interventions,
        }
      });
      dialogRef.afterClosed().subscribe(updateAppointmentData => {
        if (updateAppointmentData) {
          this.updateMeeting(updateAppointmentData)
        }
      });
    });
  }

  updateMeeting(updateAppointmentData) {
    const formattedPayload = {
      "activityId": 27,
      "appointmentId": updateAppointmentData.formValues.appointmentType.id,
      "id": this.entryData.id,
      "interventionId": updateAppointmentData.formValues.interventionLink ? updateAppointmentData.formValues.interventionLink.id : null,
      "mdate": Utility.transformDateToString(updateAppointmentData.formValues.date),
      "mtime": this.formatAppointmentTime(updateAppointmentData.formValues.timeHours, updateAppointmentData.formValues.timeMinutes),
      "notes": updateAppointmentData.isAppointmentInPast ? updateAppointmentData.formValues.meetingNotes : null,
      "other": updateAppointmentData.formValues.appointmentType.id === 47 ? updateAppointmentData.formValues.otherAppointmentName : null,
      "serviceUserId": this.serviceUserId,
      "staffId": updateAppointmentData.formValues.worker.id,
      "statusId": updateAppointmentData.isAppointmentInPast ? updateAppointmentData.formValues.attendance.id : null
    }
    this.trackTabService.updateSingleMeeting(formattedPayload).subscribe(response => {
      this.snackBarService.success(response.message.successMessage);
      this.handleUpdateAppointmentEntry.emit()
    }, error => {
      this.snackBarService.error(error.error.applicationMessage);
    })
  }

  formatAppointmentTime(timeHours, timeMinutes) {
    const timeHoursString = `${timeHours}`;
    if (timeHoursString.length === 1) {
      return `0${timeHours}:${timeMinutes}`
    } else {
      return `${timeHours}:${timeMinutes}`
    }
  }

}
