import { Router, ActivatedRoute } from '@angular/router';
import { InPageNavService } from './../../../shared/components/in-page-nav/in-page-nav.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServiceUserNavigation } from '../../service-user-nav';
import { ServiceUserService } from '../../service-user.service';
import { SnackBarService } from '../../../../framework/service/snack-bar.service';
const BASE_PATH = './service-user';

@Component({
  selector: 'app-view-user-profile',
  templateUrl: './view-user-profile.component.html',
  styleUrls: ['./view-user-profile.component.scss']
})
export class ViewUserProfileComponent implements OnInit, OnDestroy {
  user: any = {};
  fname: string;
  lname: string;
  prn: string;
  courseSize = 0;
  lddSize = 0;
  genderList;
  ethnicityRefData;
  address: any = {};
  disablity: any = {};
  parentEthnicity: string;
  establismentList;
  localAuthorityList;
  identifiers: any = {};
  sentence: any = {};
  establishmentName: string;
  localAuthorityName: string;
  llifProject: any = {};
  refStatus: any;
  refProject: any;
  refRecallReason: any;
  refWithdrawnReason: any;
  refRecInterventions: any;
  recalledDate: any;
  withdrawnDate: any;
  participantNo: any;
  dateOfPilot: any;
  dateParticipantPilot: any;
  emotionalSupport: any = {};
  employment: any = {};
  recInterventionDesc = [];
  othRecInterventionDesc = [];
  recIntSize = 0;
  othRecSize = 0;
  genderListData: any;
  profileDetailsData: any;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly serviceUserService: ServiceUserService,
    private readonly inPageNavService: InPageNavService,
    private readonly snackBarService: SnackBarService,
    private readonly serviceUserNavigation: ServiceUserNavigation) {
    this.inPageNavService.setNavItems(this.serviceUserNavigation.serviceUserSubMenu);
    this.setTitle();
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.fullName) {
        this.fname = params.fullName;

      }
      this.route.snapshot.data['title'] = `${this.fname}`;
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.id) {
        this.serviceUserService.requestDataFromMultipleSources(params.id).subscribe(responseList => {
          this.ethnicityRefData = responseList.ethnicityResp;
          this.profileDetailsData = responseList.profileDetailsResp;
          this.genderList = responseList.listOfGenderResp;
          this.establismentList = responseList.refSentanceData['refEstablishment'];
          this.localAuthorityList = responseList.refSentanceData['refLocalAuthority'];
          this.refStatus = responseList.llifProjectData['refStatus'];
          this.refRecallReason = responseList.llifProjectData['refRecallReason'];
          this.refWithdrawnReason = responseList.llifProjectData['refWithdrawalReason'];
          this.refRecInterventions = responseList.llifProjectData['refRecommendedInterventions'];
          this.refProject = responseList.llifProjectData['refProject'];;
          this.genderList.forEach(element => {
            element.value = element.description;
          });

          this.sentenceData(this.profileDetailsData);
          this.llifProjectData(this.profileDetailsData);
          this.profileDetailsData.ethnicity = this.getEthnicityName(this.profileDetailsData.ethnicityId)
          this.user = this.profileDetailsData;
          this.address = this.profileDetailsData.address;
          this.disablity = this.profileDetailsData.disabilityAndHealth;
          this.identifiers = this.profileDetailsData.identifiers;
          this.sentence = this.profileDetailsData.sentence;
          this.llifProject = this.profileDetailsData.llifProject;
          this.emotionalSupport = this.profileDetailsData.emotionalSupport;
          this.employment = this.profileDetailsData.employment;
          this.getCourseAndLddSize();
        }, error => {
          this.snackBarService.error(`${error.error.applicationMessage}`);
          this.router.navigate([BASE_PATH]);
        });
      }
    });
  }


  private llifProjectData(response: any) {
    if (response.llifProject != null) {
      this.refStatus.forEach(element => {
        element.value = element.name;
      });
      this.refRecallReason.forEach(element => {
        element.value = element.name;
      });
      this.refWithdrawnReason.forEach(element => {
        element.value = element.name;
      });
    }
  }

  private sentenceData(response: any) {
    if (response.sentence != null) {
      this.establismentList.forEach(element => {
        element.value = element.name;
      });

      this.localAuthorityList.forEach(element => {
        element.value = element.name;
      });
    }
  }

  private getCourseAndLddSize() {
    if (this.user.courseList != null || this.user.lldAndHealthProblemList != null) {
      this.courseSize = this.user.courseList.length;
      this.lddSize = this.user.lldAndHealthProblemList.length;
    }
  }

  ngOnDestroy() {
    this.inPageNavService.setNavItems(null);
  }


  onExitClicked() {
    this.router.navigate([BASE_PATH]);
  }

  onEditClicked(id): void {
    this.router.navigateByUrl('/service-user/edit/' + id);
  }
  
  onResetPasswordClicked(id){
    this.serviceUserService.resetPassword(id).subscribe(
      response => {
        this.snackBarService.success(response.successMessage);
      }, error => {
        this.snackBarService.error(`${error.error.applicationMessage}`);
      }
    );
  }

  getYesNoValue(id) {
    let value;
    if (id === 1) {
      value = 'Yes'
    } else if (id === 2) {
      value = 'No'
    } else {
      value = 'Prefer not to say'
    }
    return value;
  }

  getEthnicityName(id) {
    let ethnictiyName;
    this.ethnicityRefData.forEach(element => {
      element.refChildEthnicity.forEach(data => {
        if (data.childEthnicityId === id) {
          ethnictiyName = data.childEthnicityName;
          this.parentEthnicity = element.parentEthnicityName;
        }
      });
    });
    return ethnictiyName
  }

}
