import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManageOrganisationsComponent } from './manage-organisations.component';
import { ManageOrganisationsRoutingModule } from './manage-organisations-routing.module';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ViewOrganisationComponent } from './view-organisation/view-organisation.component';
import { AddEditOrganisationComponent } from './add-edit-organisation/add-edit-organisation.component';
import { ManageOrganisationsService } from './manage-organisations.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ManageOrganisationsRoutingModule,
    MatIconModule,
    MatFormFieldModule,
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MatMenuModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatCheckboxModule,
  ],
  declarations: [
    ManageOrganisationsComponent,
    ViewOrganisationComponent,
    AddEditOrganisationComponent,
  ],
  providers: [
    ManageOrganisationsService,
  ]
})
export class ManageOrganisationsModule { }
