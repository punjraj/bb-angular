import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { RoutePartsService } from '../../service/route-parts.service';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {
  routeParts: any[];
  routerEventSub: Subscription;

  constructor(
    private readonly router: Router,
    private readonly routePartsService: RoutePartsService,
    private readonly activeRoute: ActivatedRoute,
  ) {

    this.routerEventSub = this.router.events
    .pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe((routeChange) => {
      this.routeParts = [... this.routePartsService.generateRouteParts(this.activeRoute.snapshot)].reverse();

      this.routeParts.forEach((item, i) => {

        item.breadcrumb = this.parseText(item);
        if (item.preserveParam) {
          item.params = {};
          item.preserveParam.forEach(param => {
            item.params[param] = item.queryParams[param];
          });
        }
        item.urlSegments.forEach((urlSegment, j) => {
          if (j === 0) {
            return item.url = `${urlSegment.path}`;
          }
          item.url += `/${urlSegment.path}`;
        });

        if (i === 0) {
          return item;
        }

        item.url = `${this.routeParts[i - 1].url}/${item.url}`;

        return item;
      });
    });

  }

  ngOnInit() { }
  ngOnDestroy() {
    if (this.routerEventSub) {
      this.routerEventSub.unsubscribe();
    }
  }

  parseText(part) {
    part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
      const r = part.queryParams[b];
      return typeof r === 'string' ? r : a;
    });
    return part.breadcrumb;
  }

}
