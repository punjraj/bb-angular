import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { SessionsService } from '../../sessions/sessions.service';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { AppInsightsService } from '../../framework/service/app-insights.service';
import { ChatNotificationMatBadgeService } from './chatnotification/chatnotification-matbadge.service';
import { SrmService } from '../../features/srm/srm.service';
import { Utility } from '../../framework/utils/utility';
import { forkJoin } from 'rxjs';


@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss'],
    host: {
        '(document:click)': 'onClick()',
      }
})
export class TopnavComponent implements OnInit {

    username: string;
    appUname:string;
    authenticatedUserRoleId:string;
    pushRightClass = 'push-right';
    pageTitle:string
    passwordToExpire = false;
    chatFocus: number;
    matbaDge;
    matBadgeHidden = false;
    subscription: any;
    userOrganizations = [];
    currentOrganizationId: number = null;
    href = "";
    manageUsersRegexUrl = RegExp('service-user');
    organization:any = null ;
    userType: number = null;
    ROUTING_PATH = '/service-user';
    sideNavIcon = false;
    constructor(
        public router: Router,
        private readonly sessionService: SessionsService,
        private readonly snackBarService: SnackBarService,
        private readonly appInsightsService: AppInsightsService,
        private readonly chatNotificationMatBadgeService : ChatNotificationMatBadgeService,
        private readonly srmGoToChatService : SrmService,
        private readonly renderer: Renderer2,

    ) { 
        this.appUname = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userName;
        this.authenticatedUserRoleId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userId;
    }

    ngOnInit() {
      this.sessionService.passwordAboutToExpiredFlag.subscribe(flag => {
          this.passwordToExpire = flag;
      });
     this.passwordToExpire = JSON.parse(localStorage.getItem('passwordCheck'));

        forkJoin(
            this.sessionService.getClientOrganizations()
        ).subscribe((data:any)=>{
            this.userOrganizations = data[0];
            this.username = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userName;
            this.appInsightsService.setAuthenticatedUserContext(this.appUname, this.authenticatedUserRoleId);
            this.currentOrganizationId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).orgId;
            this.userType = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).userType;
            
        })
        this.chatNotificationMatBadgeService.chatFocus.subscribe(flag=>{
            this.chatFocus = flag
        });

        this.srmGoToChatService.getUnreadMessageCount().subscribe(count =>{
            if(count > 0){
                this.matbaDge = count;
            }else{
                this.matbaDge = 0;
                this.matBadgeHidden = true;
            }
            
        });

         this.subscription = this.chatNotificationMatBadgeService.isChatRead()
            .subscribe(isChatRead => {
                this.matbaDge = 0;
                this.matBadgeHidden = true;
         });
    }

    changePassword() {
        this.router.navigate([`/user-settings/change-password`]);
    }

    onLoggedOut() {
        this.sessionService.signout().subscribe();
    }
    
    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
        this.sideNavIcon = !this.sideNavIcon;
    }

    onClick(){
        this.renderer.removeClass(document.body, 'push-right');
        this.sideNavIcon = false;
    }

    getRoute() {
        if (this.chatFocus) {
         return 'active';
        } else {
          return '';
        }
      }
    resolveUserOrganizations() {
        this.sessionService.getClientOrganizations().subscribe(userOrganizations => {
            this.userOrganizations =  userOrganizations;
        })
    }

    onChangeEstablishment(estbId) {
        this.sessionService.changeEstablishment(estbId).subscribe(response => {
            const successString = 'Establishment switched to';
            this.currentOrganizationId = JSON.parse(atob(localStorage.getItem('token').split('.')[1])).orgId;          
            this.router.navigate([this.ROUTING_PATH]);
            this.snackBarService.success(`${successString} ${this.resolveOrgName()}`);
            const org =  Utility.getObjectFromArrayByKeyAndValue(this.userOrganizations , "id" , estbId); 
            this.organization = org.orgName;
            this.href = this.router.url;
            if (this.manageUsersRegexUrl.test(this.href)) {
                this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
                this.router.navigate([this.ROUTING_PATH]));
            }
        }, error => {
            this.snackBarService.error(error);
        })
    }

  resolveOrgName() {
    return this.userOrganizations.filter(org => org.id === this.currentOrganizationId)[0].orgName
  }

}
