import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class ChatNotificationMatBadgeService {
    matbaDge : EventEmitter<boolean> = new EventEmitter();
    chat : EventEmitter<number> = new EventEmitter();
    chatFocus:EventEmitter<number> = new EventEmitter();
    loadQuick : EventEmitter<boolean> = new EventEmitter();

    constructor(){

    }

    emitChatRead(isChatRead){
        this.matbaDge.emit(isChatRead);
    }

    emitChat(chatId){
        this.chat.emit(chatId);
    }
    
    emitChatFocus(flag){
        this.chatFocus.emit(flag);
    }
    
    getChat(){
        return this.chat;
    }

    isChatRead(){
        return this.matbaDge;
    }

    emitLoadQuick(flag){
        this.loadQuick.emit(flag);
    }

    getLoadQuick(){
        return this.loadQuick;
    }

}