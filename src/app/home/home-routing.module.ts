import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { AuthenticationGuard } from '../framework/guards/authentication.guard';
import { AuthorizationGuard } from '../framework/guards/authorization.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: { title: 'home'},
    canActivateChild: [AuthenticationGuard],
    children : [
      {
        path: 'service-user', loadChildren: () => import('../features/service-user/service-user.module').then(m => m.ServiceUserModule),
        data: { title: 'Participants', breadcrumb: '', preload: false}
      },
      {
        path: 'manage-users', loadChildren: () => import('../features/manage-users/manage-users.module').then(m => m.ManageUsersModule),
        data: { title: 'Manage Staff', breadcrumb: '', preload: false}
      },
      {
        path: 'user-settings', loadChildren: () => import('../features/user-settings/user-settings.module').then(m => m.UserSettingsModule),
        data: { title: 'User Settings', breadcrumb: '', preload: false}
      },
      {
      path: 'administration', loadChildren: () => import('../features/administration/administration.module').then(m => m.AdministrationModule),
      data: {title: '', breadcrumb: '', preload: false ,  auth: [15]}
      },
      {
        path: 'assessment-builder', loadChildren: () => import('../features/assessment/assessment.module').then(m => m.AssessmentModule),
        data: { title: 'Assessment Builder', breadcrumb: '', preload: false}
      },
      {
        path: 'content-management', loadChildren: () => import('../features/content-management/content-management.module').then(m => m.ContentManagementModule),
        data: { title: 'Manage Contents', breadcrumb: '', preload: false}
      },      {
        path: 'srm', loadChildren: () => import('../features/srm/srm.module').then(m => m.SrmModule),
        data: { title: 'Chat', breadcrumb: '', preload: false}
      },
      {
        path: 'srm', loadChildren: () => import('../features/srm/srm.module').then(m => m.SrmModule),
        data: { title: 'Chat', breadcrumb: '', preload: false}
      },
      {
        path: 'job-advert', loadChildren: () => import('../features/job-adverts/job-adverts.module').then(m => m.JobAdvertsModule),
        data: { title: 'Job Opportunities', breadcrumb: '', preload: false , auth: [17]}
      },
      {
        path: 'reports', loadChildren: () => import('../features/reports/reports.module').then(m => m.ReportsModule),
        data: { title: 'Reports', breadcrumb: '', preload: false }
      },
    ] 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
