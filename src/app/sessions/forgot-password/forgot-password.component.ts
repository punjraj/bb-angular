import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { ForgotPasswordService } from './forgot-password.service';
import { SnackBarService } from './../../framework/service/snack-bar.service';
import { ApplicationConstant } from './../../framework/constants/app-constant';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  
  forgotPasswordForm: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly forgotPasswordService: ForgotPasswordService,
    private readonly snackBar: SnackBarService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      userName: ['', Validators.required]
    });
  }

  submit() {
    const payload = {
      "appId": ApplicationConstant.ApplicationID,
      "clientId": ApplicationConstant.clientId,
      "pass": null,
      "userName": this.forgotPasswordForm.controls['userName'].value
    }
    this.forgotPasswordService.sendForgotPassword(payload).subscribe(response => {
      this.snackBar.success('An email has been sent with the next steps to reset your password.')
      this.navigateHome();
    }, error => {
      this.snackBar.error(error.error.applicationMessage);
      })
  }

  navigateHome() {
    this.router.navigateByUrl('/sessions/signin');
  }

}
