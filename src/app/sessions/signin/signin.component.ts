import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { SessionsService } from '../sessions.service';
import { MatButton } from '@angular/material/button';
import { SnackBarService } from '../../framework/service/snack-bar.service';
import { IUserMessage } from './signin.interface';
import { ApplicationConstant } from '../../framework/constants/app-constant';
import { AppConfirmService } from '../../framework/components/app-confirm/app-confirm.service';


@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss'],
    providers: [SessionsService],
})

export class SigninComponent implements OnInit {

    signinForm: FormGroup;
    signinError: IUserMessage;
    noticeBoardMessage: any;

    @ViewChild(MatButton) submitButton: MatButton;

    constructor(private readonly router: Router,
        private readonly sessionsService: SessionsService,
        private readonly builder: FormBuilder,
        private readonly snackBar: SnackBarService,
        private readonly appConfirmService: AppConfirmService

    ) { }

    ngOnInit() {
        this.initForm();
            this.sessionsService.noticeBoardMessage().subscribe((noticeBoard:any) =>{
                this.noticeBoardMessage = noticeBoard.message;
              });
    }

    signin() {

        this.signinForm.controls['pass'].setValue(btoa(this.signinForm.controls['pass'].value));
        this.sessionsService.signin(this.signinForm.value)
            .subscribe((data: HttpResponse<any>) => {
                this.router.navigate(['service-user'])
                if (data.body.isPasswordAboutToExpire) {
                    this.sessionsService.setPasswordAboutToExpireFlag(true);
                    const numberOfdaysLeft = data.body.passwordAboutToExpireDays;
                    const dialogRef = this.appConfirmService.confirm({
                      title: `Please change your password`,
                      message: `Your password will expire in ${numberOfdaysLeft} day/s.
                      Select \'OK\' to change your password now or \'Cancel\' to change it later.`,
                    });
                    dialogRef.subscribe(result => {
                      if (result) {
                        this.router.navigateByUrl('/user-settings/change-password');
                      }
                    });
                  }
            },
                (error: any) => {
                    this.snackBar.error(error.error.applicationMessage || error.message);
                    this.signinForm.controls['pass'].setValue(atob(this.signinForm.controls['pass'].value));
                    if (error.error.applicationMessageCode === 'U4006') {
                        localStorage.setItem('username', this.signinForm.controls['userName'].value);
                        this.router.navigate(['sessions/change-expired-password']);
                    }
                }
            );
    }

    initForm() {
        this.signinForm = this.builder.group({
            userName: ['', Validators.required],
            pass: ['', Validators.required],
            appId:[ApplicationConstant.ApplicationID],
            clientId:[ApplicationConstant.clientId]
        });
    }

}

