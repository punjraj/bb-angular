import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SnackBarService } from './../../framework/service/snack-bar.service';
import { ResetPasswordService } from './reset-password.service';
import { ApplicationConstant } from '../../../app/framework/constants/app-constant';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  
  private token: string;
  resetPasswordForm: FormGroup;
  action = 'forgot';

  constructor(
    public readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly snackBarService: SnackBarService,
    private readonly resetPasswordService: ResetPasswordService
  ) { }

  ngOnInit() {
    this.resolveToken();
    this.setTitle();
    this.initForm();
  }

  resolveToken() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.token) {
        this.token = params.token;
        const payload = {
          confirmPassword: null,
          encryptedToken: this.token,
          initialPassword: null
        };
        this.resetPasswordService.validatePasswordLink(payload).subscribe(response => {
          if (response == null || response['token'] == null) {
            this.snackBarService.error(`Invalid token`);
            this.navigateHome();
          }
        }, error => {
          this.snackBarService.error(error.error.applicationMessage);
         
        })
      } else {
        this.snackBarService.error(`Invalid token`);
        this.navigateHome();
      }
    });
  }

  setTitle() {
    this.route.queryParams.subscribe((params: any) => {
      if (params.action && params.action === 'create') {
        this.route.snapshot.data['title'] = 'Set Password';
        this.action = 'create';
      } else {
        this.route.snapshot.data['title'] = 'Reset Password';
      }
    });
  }

  initForm() {
    this.resetPasswordForm = this.formBuilder.group({
      encryptedToken: [this.token],
      initialPassword: ['', Validators.compose(
        [
          Validators.maxLength(36), 
          Validators.minLength(8), 
          Validators.pattern(/(?=.*\d)(?=.*[a-zA-Z])(?=.*[!\"#$%&\'\(\)\*+,-\.\/:;<=>\?@\[\\\]^_`{|}~])/)
        ]
      )],
      clientId:ApplicationConstant.clientId,
      confirmPassword: ['']
    }, { validators: this.matchingPasswords('initialPassword', 'confirmPassword') })
  }

  matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordKey];
      const passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  submit() {
    this.resetPasswordService.validateAndSavePassword(this.resetPasswordForm.value).subscribe(response => {
      if (this.action === 'create') {
        this.snackBarService.success('Your account has now been created, you may now login to the system.');
      } else {
        this.snackBarService.success('Your password has been reset, please log in.');
      }
      this.navigateHome();
    }, error => {
      this.snackBarService.error(error.error.applicationMessage);
      
    })
  }

  navigateHome() {
    this.router.navigateByUrl('/sessions/signin');
  }

}
